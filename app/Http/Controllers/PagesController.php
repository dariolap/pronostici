<?php namespace App\Http\Controllers;

use App\Category;
use App\Jobs\SendToTelegram;
use App\Mail\MailContact;
use App\Newsfeed;
use App\Phase;
use Carbon\Carbon;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use App\Event;
use App\Result;
use App\Point;
use App\User;
use App\Message;
use App\Tournament;
use App\Group;
use Auth;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class PagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Auth::user()->id != 1) {
                SendToTelegram::dispatch(Auth::user()->name, Route::getCurrentRoute()->getActionName());
                Log::stack(['access'])->info(Auth::user()->name . ' ' . Route::getCurrentRoute()->getActionName());
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resultsLastYear = Result::where('created_at', '>=', Carbon::now()->subYear()->toDateTimeString())
            ->where('user_id', auth()->user()->id)
            ->with('bet')
            ->get();

        $results = Result::where('user_id', auth()->user()->id)
            ->with('bet')
            ->get();

        $totalBets = $results->count();

        $correctBets = $results->where('win', 1)->count();

        $winPercentageRecent = 0;

        if ($totalBets != 0) {
            $winPercentageRecent = number_format(round(($correctBets / $totalBets) * 100, 2), 2);
        }

        $highestOverallOdd = number_format($results->where('win', 1)->max('bet.points'), 2);
        $highestOverallRecentOdd = number_format($resultsLastYear->where('win', 1)->max('bet.points'), 2);

        $newsfeeds = Newsfeed::limit(10)->orderBy('id', 'DESC')->get();

        return view('pages.index',
            compact('totalBets', 'winPercentageRecent', 'highestOverallOdd', 'highestOverallRecentOdd', 'newsfeeds')
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard($tournamentId)
    {
        if ($tournamentId == 1) {
            return view('pages.dashboard_world_cup_2018');
        }
        return view('pages.dashboard', [
            'newsfeeds' => Newsfeed::limit(10)
                ->where('tournament_id', $tournamentId)
                ->orderBy('id', 'DESC')
                ->get(),
            'tournament' => Tournament::find($tournamentId)
        ]);
    }

    public function results($userId = null, $tournamentId = null)
    {
        //@TODO: AGGIUNGERE TOURNAMENT_ID A RESULTS E AL POSTO DI CATEOGORY METTERE TOURNAMENTS
        if (!$userId) {
            $userId = Auth::user()->id;
        }

        $results = Result::where(['results.user_id' => $userId])
            ->select('events.name as event', 'bets.points as points', 'bets.name as bet',
                'events.deadline as deadline', 'results.win',
                'events.private as private', 'results.event_id as event_id')
            ->join('bets', 'results.bet_id', '=', 'bets.id')
            ->join('events', 'results.event_id', '=', 'events.id')
            ->orderBy('events.deadline', 'DESC');

        $category = Category::where(['id' => $tournamentId])->first();

        if ($tournamentId) {
            $results->where(['events.category_id' => $tournamentId]);
        }

        if ($results) {
            $user = User::where(['id' => $userId])
                ->first();

            return view('pages.results', [
                'results' => $results->get(),
                'user' => $user,
                'category' => $category,
                'tournament' => Tournament::find($tournamentId)
            ]);
        } else {
            throw new Exception('Something wrong');
        }
    }

    public function eventAjax(Request $request)
    {
        $results = Result::where(['results.event_id' => $request->input('event_id'), 'tournaments.id' => $request->input('tournament_id')])
            ->select('users.name as username', 'bets.points as points', 'bets.name as bet', 'events.deadline as deadline', 'results.win', 'users.avatar as avatar')
            ->join('bets', 'results.bet_id', '=', 'bets.id')
            ->join('users', 'results.user_id', '=', 'users.id')
            ->join('events', 'results.event_id', '=', 'events.id')
            ->join('categories', 'events.category_id', '=', 'categories.id')
            ->join('tournaments', 'tournaments.category_id', '=', 'tournaments.id')
            ->orderBy('users.name', 'ASC')
            ->get();

        if ($results) {
            $event = Cache::remember('event_id_' . $request->input('event_id'), 600, function () use ($request) {
                return Event::where(['id' => $request->input('event_id')])
                    ->first();
            });

            return view('pages.partials.show.event_ajax', ['results' => $results, 'event' => $event])->render();
        } else {
            throw new Exception('Something wrong');
        }

    }

    public function event($eventId, $tournamentId = null)
    {
        // @TODO, DUPLICATA, UNIRE IN UNA SOLA
        $results = Result::where(['results.event_id' => $eventId])
            ->select('users.name as username', 'bets.points as points', 'bets.name as bet', 'events.deadline as deadline', 'results.win'
            )
            ->join('bets', 'results.bet_id', '=', 'bets.id')
            ->join('users', 'results.user_id', '=', 'users.id')
            ->join('events', 'results.event_id', '=', 'events.id')
            ->orderBy('users.name', 'ASC')
            ->get();

        if ($results) {
            $event = Cache::remember('event_id_' . $eventId, 600, function () use ($eventId) {
                return Event::where(['id' => $eventId])
                    ->first();
            });

            return view('pages.event', ['results' => $results, 'event' => $event, 'tournamentId' => $tournamentId]);
        } else {
            throw new Exception('Something wrong');
        }
    }

    public function show($tournamentId)
    {
        $events = Event::select('events.*')
            ->join('categories', 'events.category_id', '=', 'categories.id')
            ->join('tournaments', 'categories.id', '=', 'tournaments.category_id')
            ->where(['tournaments.id' => $tournamentId])
            ->orderBy('events.deadline', 'ASC')
            ->orderBy('events.name', 'ASC')
            ->get();

        // Today events
        $today = collect($events)->filter(function ($item, $key) {
            return Carbon::parse($item->deadline)->isToday();
        })->groupBy(function ($item) {
            return Carbon::parse($item->deadline)->toDateTimeString();
        });

        // Future events
        $future = collect($events)->filter(function ($item, $key) {
            return (!Carbon::parse($item->deadline)->isToday() && Carbon::parse($item->deadline)->isFuture());
        })->groupBy(function ($item) {
            return Carbon::parse($item->deadline)->toDateTimeString();
        });

        // Past events: sort by deadline desc
        $past = collect($events)->filter(function ($item, $key) {
            return (!Carbon::parse($item->deadline)->isToday() && Carbon::parse($item->deadline)->isPast());
        })->sortByDesc('deadline')->groupBy(function ($item) {
            return Carbon::parse($item->deadline)->toDateTimeString();
        });

        return view('pages.show', [
            'events' => $events,
            'tournamentId' => $tournamentId,
            'today' => $today,
            'future' => $future,
            'past' => $past
        ]);
    }

    /**
     * Add new bet
     */
    public function add($tournamentId)
    {
        if (!app('App\TournamentUser')->isAuthorized(Auth::user()->id, $tournamentId)) {
            return redirect(route('pages.index'));
        }

        $events = Event::with(['Bet', 'Result' => function ($query) {
            $query->where(['user_id' => Auth::user()->id]);
        },
            'Category.Tournament' => function ($query) use ($tournamentId) {
                $query->where(['id' => $tournamentId]);
            }])
            ->where('category_id', $tournamentId)
            ->orderBy('events.deadline', 'ASC')
            ->orderBy('events.name', 'ASC')
            ->get();

        $events = Event::with(['Bet', 'Result' => function ($query) {
            $query->where(['user_id' => Auth::user()->id]);
        },
            'Category.Tournament' => function ($query) use ($tournamentId) {
                $query->where(['id' => $tournamentId]);
            }])
            ->where('category_id', $tournamentId)
            ->where(function ($query) {
                $query->where('events.deadline', '>', Carbon::now());
            })
            ->orderBy('events.deadline', 'ASC')
            ->orderBy('events.name', 'ASC')
            ->get();

        $mappedEvents = $events->groupBy(function ($item) {
            return Carbon::parse($item->deadline)->format('d/m/Y H:i');
        });

        return view('pages.add', ['mappedEvents' => $mappedEvents, 'tournamentId' => $tournamentId]);
    }

    /**
     * Show the  standings
     */
    public function standings($tournamentId)
    {
        // World cup 2018 static page
        if ($tournamentId == 1) {
            return view('pages.standings.world_cup_2018');
        }

        $tournament = Tournament::where('id', $tournamentId)->first();

        $points = Result::select(
            'results.user_id',
            'users.name',
            DB::raw('SUM(CASE WHEN results.win = 1 THEN bets.points ELSE 0 END) as points'),
            DB::raw('SUM(CASE WHEN results.win != 0 THEN 1 ELSE 0 END)  as total_results'),
            DB::raw('SUM(CASE WHEN results.win = 1 THEN 1 ELSE 0 END) as total_wins')
        )
            ->join('bets', 'results.bet_id', '=', 'bets.id')
            ->join('events', 'results.event_id', 'events.id')
            ->join('users', 'results.user_id', '=', 'users.id')
            ->join('categories', 'events.category_id', '=', 'categories.id')
            ->join('tournaments', function (JoinClause $join) use ($tournamentId) {
                $join->on('tournaments.category_id', '=', 'categories.id')
                    ->where('tournaments.id', $tournamentId)
                    ->where(function ($query) {
                        $query->where('tournaments.deadline_subscription', '<', Carbon::now()->toDateTimeString())
                            ->orWhereNull('tournaments.deadline_subscription');
                    });
            })
            ->groupBy('results.user_id')
            ->orderByDesc('points')
            ->get();

        $phases = Phase::with(['groups' => function ($query) use ($tournamentId) {
            $query
                ->with(['users.points' => function ($query) use ($tournamentId) {
                    $query
                        ->orderBy('points.points', 'DESC')
                        ->where(['points.tournament_id' => $tournamentId]);
                }]);
        }])
            ->where(['tournament_id' => $tournamentId])
            ->orderBy('phases.phase', 'ASC')
            ->get();

        $results = Result::select('results.user_id', 'events.deadline', 'users.name as name', 'bets.points', 'results.win')
            ->join('bets', 'results.bet_id', '=', 'bets.id')
            ->join('users', 'results.user_id', '=', 'users.id')
            ->join('events', 'bets.event_id', '=', 'events.id')
            ->join('categories', 'events.category_id', '=', 'categories.id')
            ->join('tournaments', 'tournaments.category_id', '=', 'categories.id')
            ->where(['tournaments.id' => $tournamentId])
            ->get();


        $users = User::join('tournaments_users', 'tournaments_users.user_id', 'users.id')
            ->where(['tournaments_users.tournament_id' => $tournamentId])
            ->get();

        $maxBet = Cache::remember('best_bet_' . $tournamentId, 60, function () use ($tournamentId) {
            return DB::table('results')
                ->select('events.name as event', 'bets.name as bet_type', 'bets.points', 'users.name as name', 'users.avatar as avatar')
                ->join('users', 'results.user_id', '=', 'users.id')
                ->join('bets', 'results.bet_id', '=', 'bets.id')
                ->join('events', 'results.event_id', '=', 'events.id')
                ->join('categories', 'events.category_id', '=', 'categories.id')
                ->join('tournaments', 'categories.id', '=', 'tournaments.category_id')
                ->whereIn('bets.points', function ($query) use ($tournamentId) {
                    $query->selectRaw('max(bets.points)')
                        ->from('bets')
                        ->join('results', 'results.bet_id', '=', 'bets.id')
                        ->join('users', 'results.user_id', '=', 'users.id')
                        ->join('events', 'results.event_id', '=', 'events.id')
                        ->join('categories', 'events.category_id', '=', 'categories.id')
                        ->join('tournaments', 'categories.id', '=', 'tournaments.category_id')
                        ->where(['win' => 1, 'tournaments.id' => $tournamentId]);
                })
                ->where(['win' => 1, 'tournaments.id' => $tournamentId])
                ->get();
        });

        $bestWinCount = Cache::remember('best_win_' . $tournamentId, 10, function () use ($tournamentId) {

            return DB::table('results')
                ->selectRaw('count(results.win) as c, results.user_id, users.name, users.avatar')
                ->join('users', 'results.user_id', '=', 'users.id')
                ->join('events', 'results.event_id', 'events.id')
                ->join('categories', 'events.category_id', '=', 'categories.id')
                ->join('tournaments', 'categories.id', '=', 'tournaments.category_id')
                ->where(['results.win' => 1, 'tournaments.id' => $tournamentId])
                ->groupBy('results.user_id')
                //@TODO: trovare metodo migliore
                ->havingRaw('COUNT(win) = (select COUNT(win) AS c
             FROM
                 results
             INNER JOIN
                  users ON results.user_id = users.id
             INNER JOIN
                  events ON results.event_id = events.id
             INNER JOIN
                  categories ON events.category_id = categories.id
             INNER JOIN
                  tournaments ON categories.id = tournaments.category_id
             WHERE
                 results.win = 1 and tournaments.id = ' . $tournamentId . '
             GROUP BY results.user_id
             ORDER BY c DESC
             LIMIT 1)')
                ->get();
        });

        return view('pages.standings.index', [
            'tournament' => $tournament,
            'points' => $points,
            'groupsPhases' => Collect($phases)->where('type_id', 1)->sortByDesc('id'),
            'knockoutPhases' => Collect($phases)->where('type_id', 2),
            'maxBet' => $maxBet,
            //   'bestWoman' => $bestWoman,
            'bestWinCount' => $bestWinCount,
            'results' => $results,
            'users' => $users,
        ]);
    }

    public function store($tournamentId, Request $request)
    {
        foreach ($request->input('results') as $index => $value) {
            $event = Event::where(['id' => $index])
                ->where('deadline', '>=', \Illuminate\Support\Carbon::now())
                ->first();

            if ($event) {
                // Controllo se risultato già esiste
                $result = Result::where([
                    'event_id' => $index,
                    'user_id' => Auth::user()->id])
                    ->first();
                $save = 1;
                if ($result) {
                    if ($result->bet_id != $value) {
                        $result->bet_id = $value;
                    } else {
                        // Non è cambiato nulla, non salvo
                        $save = 0;
                    }
                } else {
                    $result = new Result([
                        'bet_id' => $value,
                        'event_id' => $index,
                        'user_id' => Auth::user()->id,
                    ]);
                }
                if ($save) {
                    $result->save();
                }
            }
        }
        return redirect(route('pages.show', $tournamentId));
    }

    /*
     * Messages
     */
    public function messages($tournamentId)
    {
        if (!app('App\TournamentUser')->isAuthorized(Auth::user()->id, $tournamentId)) {
            return redirect(route('pages.index'));
        }

        $messages = Message::with(['User'])
            ->where(['tournament_id' => $tournamentId])
            ->orderBy('id', 'desc')
            ->get();

        return view('pages.messages', ['messages' => $messages, 'tournamentId' => $tournamentId]);
    }

    /**
     * store message
     */
    public function store_message($tournamentId, Request $request)
    {
        $message = new Message([
            'message' => $request->input('message'),
            'user_id' => Auth::user()->id,
            'tournament_id' => $tournamentId
        ]);

        $message->save();

        return redirect(route('pages.messages', $tournamentId));
    }

    public function tournaments_available()
    {
        return view('pages.tournaments_available', ['tournaments' => Tournament::getAvailable()]);
    }

    public function old_tournaments()
    {
        return view('pages.old_tournaments',
            [
                'tournaments' => Tournament::whereDate('deadline', '<', Carbon::parse('now')->format('Y-m-d'))
                    ->orderBy('id', 'DESC')
                    ->get()
            ]);

    }

    public function champions_list()
    {
        $tournaments = Tournament::all();
        $points = Point::all();

        return view('pages.champions_list', [
            'tournaments' => $tournaments,
            'points' => $points,
        ]);
    }

    public function general_regulation()
    {
        return view('pages.general_regulation');
    }

    public function tournament_regulation($tournamentId)
    {
        $news = Newsfeed::where('is_regulation', 1)
            ->where('tournament_id', $tournamentId)
            ->orderByDesc('id')
            ->first();

        return view('pages.tournament_regulation', compact('news'));
    }

    /**
     * Mostra la pagina del modulo di contatto.
     */
    public function contact_us()
    {
        return view('pages.contact_us');
    }

    /**
     * Gestisce l'invio del modulo di contatto.
     */
    public function send_contact_mail(Request $request)
    {
        // Validazione dei dati
        $request->validate([
            'message' => 'required|string|max:2000',
        ]);

        // Recupera l'utente autenticato
        $user = auth()->user();

        // Chiave univoca per la cache basata sull'utente
        $cacheKey = "contact_message_{$user->id}";

        // Controlla se l'utente ha già inviato un messaggio oggi
        if (cache()->has($cacheKey)) {
            Log::info(__('custom.one_message_per_day'));
            Log::info('user: ' . $user->email);
            return redirect()->route('pages.contact_us')->with(
                'error',
                __('custom.one_message_per_day')
            );
        }

        // Validazione dei dati del form
        $request->validate([
            'message' => 'required|string',
        ]);

        try {
            // Dati del form
            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'message' => $request->input('message'),
                'subject' => __('custom.user_message_subject'),
                'ip' => $request->ip(),
            ];

            // Invia la mail
            Mail::to(config('mail.from.address'))->send(new MailContact($data));

            // Imposta il limite nella cache per 1 giorno
            cache()->put($cacheKey, true, now()->addDay());

            // Redirect con messaggio di successo
            return redirect()->route('pages.contact_us')->with('message', __('custom.email_sent_success'));
        } catch (\Exception $e) {
            // Log in caso di errore (opzionale)
            Log::error('Errore nell\'invio dell\'email: ' . $e->getMessage());

            // Redirect con messaggio di errore
            return redirect()->route('pages.contact_us')->with(
                'error',
                __('custom.email_send_error')
            );
        }
    }
}
