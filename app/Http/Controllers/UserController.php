<?php namespace App\Http\Controllers;

use App\Rules\ReCaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Validator;
use App\User;
use Illuminate\Support\Facades\Session;
use Auth;
use Lang;
use Telegram\Bot\Laravel\Facades\Telegram;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('pages.index'));
        }
        return view('login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect(route('landing.index'));
    }

    public function checkLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3',
        ]);

        $userData = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];

        /*if ($request->get('email') == 'federico.ciufoli@libero.it') {
            Telegram::sendMessage([
                'chat_id' => '23463942',
                'parse_mode' => 'HTML',
                'text' => 'Federico'
            ]);
            Session::flash('message', 'Account eliminato per abbandono ingiustificato del torneo.');
            return back();
        }
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3',
        ]);*/

        if (Auth::attempt($userData)) {
            return redirect(route('pages.index'));
        } else {
            Session::flash('message', Lang::get('auth.failed'));
            return back()->with('error', 'login')->withInput($request->only(['email']));
        }
    }

    /**
     * Display a listing of the resource.
     * @deprecated
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request)
    {
        return view('signup');
    }

    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users,name',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:3',
            'password_repeat' => 'same:password|required|min:3',
            'full_name' => 'nullable|string|size:0', // Honeypot
            'g-recaptcha-response' => ['required', new ReCaptcha]
        ], [
            'password_repeat' => 'Ripeti password',
        ]);

        if ($validator->fails()) {
            Session::flash('message', Lang::get('auth.invalid_signup'));
            return back()->with('error', 'signup')
                ->withInput($request->only(['name', 'email']))
                ->withErrors($validator);
        }

        // Verifica honeypot
        if (!empty($request->input('full_name'))) {
            Log::info('Bot detected: honeypot field filled.');
            return back()->with('error', 'signup')
                ->withErrors('Bot detected.');
        }

        // Verifica tempo di invio
        $formStartTime = $request->input('form_start_time');
        if (now()->timestamp - $formStartTime < 5) {
            Log::info('Form signup submitted too quickly. Possible bot.');
            Log::info($request->header('User-Agent'));
            return back()->with('error', 'signup')
                ->withErrors('Form submitted too quickly. Possible bot.');
        }

        $request->request->add(['role_id' => 1]);

        Log::info('Creato Utente ' . $request->input('email'));

        User::create($request->all());

        Session::flash('success', trans('adminlte.signup_success'));

        // redirect login
        return redirect(route('login.get'));
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
