<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Validator;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        return view('user.profiles.edit', [
            'user' => Auth()->user()
        ]);
    }

    public function update(UserRequest $request)
    {
        $user = Auth()->user();

        $user->fill(array_merge($request->except('avatar'), [
            'avatar' => $this->_uploadAvatar($request, $user)
        ]))->save();

        return back();
    }

    protected function _uploadAvatar(UserRequest $request, $user)
    {
        $avatar = $user->avatar;
        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            if (!$avatar || $avatar == config('custom.default_user_image')) {
                $avatar = Carbon::now()->getTimestamp() . '_' . Uuid::generate() . '.' . $request->file('avatar')->extension();
            }
            $request->file('avatar')->move(
                base_path() . '/public/storage/uploads/avatars/', $avatar
            );
        }
        return $avatar;
    }
}
