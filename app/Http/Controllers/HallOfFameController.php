<?php

namespace App\Http\Controllers;

use App\HallOfFame;
use App\Http\Controllers\Controller;
use App\Http\Requests\HallOfFameRequest;
use App\Tournament;
use App\User;
use Illuminate\Http\Request;

class HallOfFameController extends Controller
{
    public function index()
    {
        $hallOfFames = HallOfFame::all();

        // Raccogli tutti gli user_ids dai best_odd_wins
        $userIds = collect($hallOfFames)
            ->pluck('best_odd_wins.*.user_ids') // Estrae tutti gli ID utente
            ->flatten() // Appiattisce la collezione in un unico array
            ->unique(); // Rimuove eventuali duplicati

        // Recupera i nomi degli utenti in base agli ID
        $bestOddWinUsers = User::whereIn('id', $userIds)->pluck('name', 'id');

        return view('hall_of_fame.index', compact('hallOfFames', 'bestOddWinUsers'));
    }

}
