<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Phase;
use Illuminate\Http\Request;
use App\Category;
use App\Tournament;
use Auth;
use Webpatser\Uuid\Uuid;

class TournamentController extends Controller
{
    public function index()
    {
        return view('admin.tournaments.index', [
            'tournaments' => Tournament::orderBy('id', 'DESC')->get()
        ]);
    }

    public function add()
    {
        return view('admin.tournaments.add', [
            'categories' => Category::orderBy('id', 'DESC')->get()
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:32',
            'short_name' => 'required|string|max:16',
            'category_id' => 'required|numeric|exists:categories,id',
            'deadline' => 'required|date_format:"Y-m-d H:i"',
            'deadline_subscription' => 'nullable|date_format:"Y-m-d H:i"',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $data = $request->all();
        $data['logo'] = $this->_uploadLogo($request);
        $data['user_id'] = Auth::user()->id;

        Tournament::create($data);

        return redirect(route('admin.tournaments.index'))->with('message', trans('custom.tournament_added'));
    }

    public function edit(Request $request, Tournament $tournament)
    {
        return view('admin.tournaments.edit', [
            'categories' => Category::all(),
            'tournament' => $tournament,
        ]);
    }

    public function update(Request $request, Tournament $tournament)
    {
        $this->validate($request, [
            'name' => 'required|string|max:32',
            'short_name' => 'required|string|max:16',
            'category_id' => 'required|numeric|exists:categories,id',
            'deadline' => 'required|date_format:"Y-m-d H:i"',
            'deadline_subscription' => 'nullable|date_format:"Y-m-d H:i"',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $tournament->name = $request->input('name');
        $tournament->short_name = $request->input('short_name');
        $tournament->logo = $this->_uploadLogo($request, $tournament);
        $tournament->category_id = $request->input('category_id');
        $tournament->deadline = $request->input('deadline');
        $tournament->user_id = Auth::user()->id;

        $tournament->save();

        return redirect(route('admin.tournaments.index'))->with('message', trans('custom.tournament_updated'));
    }

    protected function _uploadLogo(Request $request, $tournament = false)
    {
        $logo = null;

        if ($tournament) {
            $logo = $tournament->logo;
        }

        if ($request->hasFile('logo') && $request->file('logo')->isValid()) {
            if (!$logo || $logo == config('custom.default_tournament_logo')) {
                $logo = \Carbon\Carbon::now()->getTimestamp() . '_' . Uuid::generate() . '.' . $request->file('logo')->extension();
            }
            $request->file('logo')->move(
                base_path() . '/public/storage/uploads/tournaments/', $logo
            );
        }
        else {
            return null;
        }

        if (!$logo) {
            // @TODO Custom Exception
            abort(404);
        }

        return $logo;
    }

    public function destroy(Tournament $tournament)
    {
        // Controlla se il torneo ha fasi associate
        if ($tournament->phase()->count() > 0) {
            return redirect(route('admin.tournaments.index'))->with('error', trans('custom.tournament_has_phases'));
        }

        // Se esiste un logo associato, eliminalo dal server
        if ($tournament->logo && $tournament->logo != config('custom.default_tournament_logo')) {
            $logoPath = base_path() . '/public/storage/uploads/tournaments/' . $tournament->logo;
            if (file_exists($logoPath)) {
                unlink($logoPath); // Elimina il file del logo
            }
        }

        // Elimina il torneo
        $tournament->delete();

        return redirect(route('admin.tournaments.index'))->with('message', trans('custom.tournament_deleted'));
    }

    public function getPhasesAndUsers(Tournament $tournament)
    {
        // Recupera gli utenti associati al torneo tramite la relazione belongsToMany
        $users = $tournament->users;

        // Recupera le fasi in base al tournament_id
        $phases = Phase::where('tournament_id', $tournament->id)->get();

        // Restituisci un JSON con entrambe le liste
        return response()->json([
            'users' => $users,
            'phases' => $phases
        ]);
    }
}
