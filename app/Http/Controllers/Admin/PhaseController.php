<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Phase;
use App\PhaseType;
use Illuminate\Http\Request;
use App\Tournament;
use Auth;
use Illuminate\Support\Facades\Validator;

class PhaseController extends Controller
{
    public function index()
    {
        return view('admin.phases.index', [
            'phases' => Phase::orderBy('id', 'DESC')->get()
        ]);
    }

    public function add()
    {
        return view('admin.phases.add', [
            'types' => PhaseType::all(),
            'tournaments' => Tournament::orderByDesc('id')->get()
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'phase' => 'required|numeric',
            'tournament_id' => 'required|numeric|exists:tournaments,id',
            'type_id' => 'required|numeric|exists:phase_types,id',
            'n_qualified' => 'required|numeric',
            'n_playoff' => 'nullable|numeric',
            'start_date' => 'required|date_format:"Y-m-d H:i"',
            'end_date' => 'required|date_format:"Y-m-d H:i"',
        ]);

        $phase = Phase::create($request->all());

        if ($phase) {
            return redirect()->route('admin.phases.index')->with('message',trans('custom.phase_added'));
        }

        return redirect()->route('admin.phases.add')->with('fail', trans('custom.error_occurred'));
    }

    public function edit(Request $request, Phase $phase)
    {
        return view('admin.phases.edit', [
            'phase' => $phase,
            'types' => PhaseType::all(),
            'tournaments' => Tournament::orderByDesc('id')->get()
        ]);
    }

    public function update(Request $request, Phase $phase)
    {
        // validate
        $validator = Validator::make($request->all(), [
            'phase' => 'required|numeric',
            'tournament_id' => 'required|numeric|exists:tournaments,id',
            'n_qualified' => 'required|numeric',
            'n_playoff' => 'nullable|numeric',
            'start_date' => 'required|date_format:"Y-m-d H:i"',
            'end_date' => 'required|date_format:"Y-m-d H:i"',
        ]);

        // check validation
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with('fail', trans('custom.error_occurred'));
        }

        $phase->update($request->all());

        return redirect()->route('admin.phases.index')->with('message', trans('custom.phase_updated'));
    }

    public function destroy(Phase $phase)
    {
        // Controlla se la fase ha gruppi associati
        if ($phase->groups()->count() > 0) {
            return redirect(route('admin.phases.index'))->with('error', trans('custom.phase_has_groups'));
        }

        // Elimina la fase
        $phase->delete();

        return redirect(route('admin.phases.index'))->with('message', trans('custom.phase_deleted'));
    }

}
