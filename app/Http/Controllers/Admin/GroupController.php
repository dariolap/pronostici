<?php namespace App\Http\Controllers\Admin;

use App\Group;
use App\Http\Controllers\Controller;
use App\Phase;
use Illuminate\Http\Request;
use App\Tournament;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index()
    {
        return view('admin.groups.index', [
            'groups' => Group::orderBy('id', 'DESC')->get()
        ]);
    }

    public function add()
    {
        // Tournaments Available
        $tournaments = Tournament::where('deadline', '>=', Carbon::now())
            ->get();

        return view('admin.groups.add', compact('tournaments'));
    }

    /**
     * @todo rivedere codice e nomi variabili
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // Validazione dei dati
        $request->validate([
            'tournament_id' => 'required|exists:tournaments,id', // Verifica che il torneo esista
            'group.*.user_id' => 'required|exists:users,id', // Assicura che l'utente esista
            'group.*.phase_id' => 'required|exists:phases,id', // Assicura che la fase esista
            'group.*.group' => 'required|string|max:255', // Assicura che il nome del gruppo sia presente
            'group.*.bonus' => 'nullable|numeric|min:0', // Bonus è opzionale, deve essere numerico se presente
        ], [
            'tournament_id.required' => 'Il campo torneo è obbligatorio.',
            'tournament_id.exists' => 'Il torneo selezionato non è valido.',
            'group.*.user_id.required' => 'Il campo utente è obbligatorio.',
            'group.*.user_id.exists' => 'L\'utente selezionato non è valido.',
            'group.*.phase_id.required' => 'Il campo fase è obbligatorio.',
            'group.*.phase_id.exists' => 'La fase selezionata non è valida.',
            'group.*.group.required' => 'Il campo nome del gruppo è obbligatorio.',
            'group.*.group.max' => 'Il nome del gruppo non può superare i 255 caratteri.',
            'group.*.bonus.numeric' => 'Il bonus deve essere un numero.',
            'group.*.bonus.min' => 'Il bonus deve essere almeno 0.',
        ]);

        $success = true;

        DB::beginTransaction();
        try {

            foreach ($request->input('group') as $data) {
            // Prepara i dati per il gruppo corrente
            $groupData = [
                'phase_id' => $data['phase_id'],
                'group' => $data['group'],
            ];

            // Controlla se esiste già un gruppo con phase_id e nome uguale
            $group = Group::firstOrCreate([
                'phase_id' => $data['phase_id'],
                'group' => $data['group'],
            ]);

            // Preparazione dei dati per il sync degli utenti
            $groupUsers = [];
          //  foreach ($data['user_id'] as $userId) {
                $groupUsers[$data['user_id']] = ['bonus' => $data['bonus'] ?? 0];
          //  }

            // Aggiorna gli utenti associati al gruppo senza rimuovere quelli esistenti
            $group->users()->syncWithoutDetaching($groupUsers);

            }

            DB::commit();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            $success = false;
        }

        if ($success) {
            return redirect(route('admin.groups.index'))->with('message', trans('custom.group_updated'));
        }
        return redirect(route('admin.groups.add'))->with('fail',trans('custom.error_occurred'));
    }

    public function edit(Group $group)
    {
        $tournament = $group->phase->tournament;
        $users = $tournament->users;
        $phases = Phase::where('tournament_id', $tournament->id)->get();

        return view('admin.groups.edit', [
            'group' => $group,
            'tournament' => $tournament,
            'phases' => $phases,
            'users' => $users,
        ]);
    }

    public function update(Request $request, Group $group)
    {
        // Validazione dei dati
        $request->validate([
            'user_ids' => 'required|array|min:1', // Deve essere un array di user_ids
            'user_ids.*' => 'exists:users,id',
            'phase_id' => 'required|exists:phases,id',
            'group' => 'required|string|max:255',
            'bonus' => 'nullable|numeric|min:0',
        ], [
            'user_ids.required' => 'Il campo utenti è obbligatorio.',
            'user_ids.exists' => 'Uno o più utenti selezionati non sono validi.',
            'phase_id.required' => 'Il campo fase è obbligatorio.',
            'phase_id.exists' => 'La fase selezionata non è valida.',
            'group.required' => 'Il campo nome del gruppo è obbligatorio.',
            'group.max' => 'Il nome del gruppo non può superare i 255 caratteri.',
            'bonus.numeric' => 'Il bonus deve essere un numero.',
            'bonus.min' => 'Il bonus deve essere almeno 0.',
        ]);

        $success = true;

        DB::beginTransaction();
        try {
            // Prepara i dati per il gruppo corrente
            $groupData = [
                'phase_id' => $request->input('phase_id'),
                'group' => $request->input('group'),
            ];

            $group->update($groupData);

            // Preparazione dei dati per l'aggiornamento degli utenti
            $groupUsers = [];
            foreach ($request->input('user_ids') as $userId) {
                $groupUsers[$userId] = ['bonus' => $request->input('bonus') ?? 0];
            }

            // Sincronizza gli utenti, eliminando quelli precedenti e aggiungendo i nuovi
            $group->users()->sync($groupUsers);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
        }

        if ($success) {
            return redirect(route('admin.groups.index'))->with('message', trans('custom.group_updated'));
        }

        return redirect(route('admin.groups.edit', $group->id))->with('fail', trans('custom.error_occurred'));
    }


    public function destroy(Group $group)
    {
        $group->delete();

        return redirect(route('admin.groups.index'))->with('message', trans('custom.group_deleted'));
    }
}
