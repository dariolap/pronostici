<?php

namespace App\Http\Controllers\Admin;

use App\HallOfFame;
use App\Http\Controllers\Controller;
use App\Http\Requests\HallOfFameRequest;
use App\Tournament;
use App\User;
use Illuminate\Http\Request;

class HallOfFameController extends Controller
{
    public function index()
    {
        $hallOfFames = HallOfFame::all();

        // Raccogli tutti gli user_ids dai best_odd_wins
        $userIds = collect($hallOfFames)
            ->pluck('best_odd_wins.*.user_ids') // Estrae tutti gli ID utente
            ->flatten() // Appiattisce la collezione in un unico array
            ->unique(); // Rimuove eventuali duplicati

        // Recupera i nomi degli utenti in base agli ID
        $bestOddWinUsers = User::whereIn('id', $userIds)->pluck('name', 'id');

        $title = 'Delete User!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);

        return view('admin.hall_of_fame.index', compact('hallOfFames','bestOddWinUsers'));
    }

    public function create()
    {
        $tournaments = Tournament::select('tournaments.*')
            ->leftJoin('hall_of_fames', 'tournaments.id', '=', 'hall_of_fames.tournament_id')
            ->whereNull('hall_of_fames.id')
            ->get();

        $users = User::orderBy('name')->get();

        // Ritorna una vista per creare un nuovo HallOfFame
        return view('admin.hall_of_fame.add', compact('tournaments', 'users'));
    }

    public function store(HallOfFameRequest $request)
    {
        HallOfFame::create($request->all());

        return redirect()->route('admin.hall_of_fames.index')->with('success', __('custom.store.success'));
    }

    public function edit($id)
    {
        $tournaments = Tournament::select('tournaments.*')
            ->leftJoin('hall_of_fames', 'tournaments.id', '=', 'hall_of_fames.tournament_id')
            ->whereNull('hall_of_fames.id')
            ->orWhere('hall_of_fames.id', $id)
            ->get();

        $users = User::orderBy('name')->get();

        $hallOfFame = HallOfFame::findOrFail($id);

        return view('admin.hall_of_fame.edit', compact('hallOfFame', 'tournaments', 'users'));
    }

    public function update(HallOfFameRequest $request, $id)
    {
        $hallOfFame = HallOfFame::findOrFail($id);

        $hallOfFame->update($request->all());
        return redirect()->route('admin.hall_of_fames.index')->with('success', __('custom.update.success'));
    }

    public function destroy(HallOfFame $hallOfFame)
    {
        $hallOfFame->delete();

        return redirect()->route('admin.hall_of_fames.index')->with('success', __('custom.destroy.success'));
    }

    public function addHighestOddWinRow(Request $request)
    {
        $users = User::orderBy('name')->get();

        $key = $request->input('key');

        return view('admin.hall_of_fame.partials.highest_odd_win_row', compact('users', 'key'))->render();
    }
}
