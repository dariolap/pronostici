<?php namespace App\Http\Controllers\AjaxController;

use App\Comment;
use Illuminate\Http\Request;
use Auth;

class AjaxCommentController extends AjaxBaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function store(Request $request)
    {
        $this->validate($request, [ 'comment' => 'required' ]);

        $comment = new Comment([
            'comment' => $request->input('comment'),
            'user_id' => Auth::user()->id,
            'newsfeed_id' => $request->input('newsfeed_id'),
        ]);

        if ($comment->save()) {
            return $this->response_composer(true,  trans('custom.comment_success'));
        }
        return $this->response_composer(false, trans('custom.error_occurred'));
       
    }

    public function delete(Request $request)
    {
        $comment = Comment::find($request->input('comment_id'));

        if ($comment && $comment->delete_authorized(Auth::user()->id)) {
            $comment->delete();
            return $this->response_composer(true,  trans('custom.comment_deleted'));
        }
        return $this->response_composer(false, trans('custom.error_occurred'));

    }
}
