<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    use SoftDeletes;

    protected $table = 'comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'newsfeed_id', 'comment',
    ];

    public function newsfeed()
    {
        return $this->belongsTo('App\NewsFeed');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function delete_authorized($userId) {
        $user = User::find($userId);

        if ($this->user_id == Auth::user()->id || $user->isAdmin()) {
            return true;
        }
        return false;
    }
}
