<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $table = 'points';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id', 'points', 'user_id', 'last_update'
    ];

    /**
     * Get the user record associated with the bet.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
