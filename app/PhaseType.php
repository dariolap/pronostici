<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhaseType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "phase_types";
    protected $fillable = [
        'title',
    ];

   public function phases()
    {
        return $this->hasMany('App\Phases', 'id');
    }
}
