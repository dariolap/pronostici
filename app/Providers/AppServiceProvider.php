<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use Auth;
use App\Http\ViewComposers\AdminLteComposer;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{

    public function __construct()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events, \Illuminate\Http\Request $request)
    {
        View::composer('page', AdminLteComposer::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
