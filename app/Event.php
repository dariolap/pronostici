<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'name', 'deadline',
        'private', 'calculated', 'final_summary'
    ];

    /**
     * Get the category record associated with the event.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the phone bet associated with the event.
     */
    public function bet()
    {
        return $this->hasMany('App\Bet');
    }

    /**
     * Get the event record associated with the result.
     */
    public function result()
    {
        return $this->hasMany('App\Result');
    }
}
