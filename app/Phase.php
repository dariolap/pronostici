<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
     protected $table = 'phases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phase', 'tournament_id', 'name',
        'type_id', 'n_qualified', 'n_playoff',
        'start_date', 'end_date', 'notes'
    ];

    /**
     * Get the tournament record associated with the group.
     */
    public function tournament()
    {
        return $this->belongsTo('App\Tournament');
    }

    /**
     * Get the groups record associated with the the group.
     */
    public function groups()
    {
        return $this->hasMany('App\Group');
    }

    /**
     * Get the type of the phase
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\PhaseType', 'type_id', 'id');
    }
}
