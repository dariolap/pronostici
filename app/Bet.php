<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    protected $table = 'bets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'points', 'event_id'
    ];
    
    /**
     * Get the event record associated with the bet.
     */
    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    /**
     * Get the result record associated with the bet.
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * Get the user record associated with the bet.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
