<?php
if (! function_exists('reloadBetExtractCorrectScore')) {
    function reloadBetExtractCorrectScore($text, $teamA, $teamB)
    {
        // Usa un'espressione regolare per trovare il pattern numero-trattino-numero
        if (preg_match('/\b(\d+-\d+)\b/', $text, $matches)) {
            $score = $matches[1];

            // Controlla se il punteggio è valido (da 0-0 a 4-4)
            if (preg_match('/^[0-4]-[0-4]$/', $score)) {
                // Se il nome del team precede lo score, verifica
                if (strpos($text, $teamB) !== false) {
                    // Inverti il risultato se il teamB è menzionato
                    [$scoreA, $scoreB] = explode('-', $score);
                    return "$scoreB-$scoreA";
                }
                // Se non è teamB o è teamA o altra parola (es. "pareggio"), restituisci il risultato così com'è
                return $score;
            }
        }

        return null; // Restituisci null se il pattern non viene trovato o non è valido
    }
}
