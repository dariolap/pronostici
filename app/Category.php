<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get the event record associated with the category.
     */
    public function event()
    {
        return $this->hasMany('App\Event');
    }

    /**
     * Get the category record associated with the category.
     */
    public function tournament()
    {
        return $this->belongsTo('App\Tournament');
    }
}
