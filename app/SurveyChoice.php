<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyChoice extends Model
{
    protected $table = 'surveys_choices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'response'
    ];

    /**
     * Get the survey record associated with the choice.
     */
    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    /**
     * Get the survey_results records associated with the choice.
     */
    public function survey_results()
    {
        return $this->hasMany('App\SurveyResult', 'choice_id');
    }


}

