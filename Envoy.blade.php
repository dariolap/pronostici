@servers(['prod' => 'deployer@172.31.43.255'])

@task('deploy_prod', ['on' => ['prod']])
cd /var/www/html/pronostici
git pull origin master
composer update
php artisan dump-autoload
php artisan migrate
@endtask
