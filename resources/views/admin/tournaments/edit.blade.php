@extends('page')

@section('content_header')
    <h1>Admin - {{ __('custom.edit_tournament') }}</h1>
@stop

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/plugin/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/plugin/selectize/selectize.css') }}">
@stop


@section('title', 'The Betting Game - Modifica torneo')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <form action="{{ url(route('admin.tournaments.update', ['tournament' => $tournament->id])) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group has-feedback">
                            <label for="name">{{ trans('custom.name') }}</label>
                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.name') }}" value="{{ old('name', $tournament->name) }}">
                            @if ($errors->has('name'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group has-feedback">
                            <label for="short_name">{{ trans('custom.short_name') }}</label>
                            <input type="text" name="short_name" class="form-control {{ $errors->has('short_name') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.short_name') }}" value="{{ old('short_name', $tournament->short_name) }}">
                            @if ($errors->has('short_name'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('short_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group has-feedback">
                            <label for="category_id">{{ trans('custom.category') }}</label>
                            <select name="category_id" class="selectize {{ $errors->has('category_id') ? 'is-invalid' : '' }}">
                                @foreach ($categories as $c)
                                    <option value="{{ $c->id }}" {{ old('category_id', $tournament->category_id) == $c->id ? 'selected' : '' }}>
                                        {{ $c->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group has-feedback">
                            <label for="deadline">{{ trans('custom.deadline') }}</label>
                            <input type="text" name="deadline" class="form-control datetimepicker {{ $errors->has('deadline') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.deadline') }}" value="{{ old('deadline', $tournament->deadline ? \Carbon\Carbon::parse($tournament->deadline)->format('Y-m-d H:i') : '') }}">
                            @if ($errors->has('deadline'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('deadline') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group has-feedback">
                            <label for="deadline_subscription">{{ trans('custom.deadline_subscription') }}</label>
                            <input type="text" name="deadline_subscription" class="form-control datetimepicker {{ $errors->has('deadline_subscription') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.deadline_subscription') }}" value="{{ old('deadline_subscription', $tournament->deadline_subscription ? \Carbon\Carbon::parse($tournament->deadline_subscription)->format('Y-m-d H:i') : '') }}">
                            @if ($errors->has('deadline_subscription'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('deadline_subscription') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>{{ __('custom.logo') }}</label>
                            <input id="file" type="file" class="form-control {{ $errors->has('logo') ? 'is-invalid' : '' }}" name="logo" accept="image/*">
                            <span class="text-danger file-error">
                                @if ($errors->has('logo'))
                                    {{ $errors->first('logo') }}
                                @endif
                            </span>
                            @if($tournament->logo)
                                <p class="mt-2 text-center">
                                    <img id="logo" style="width: 100px; height: 100px; margin-bottom:10px" class="img-circle" src="{{ $tournament->getLogo() }}">
                                </p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                        <button type="submit"
                                class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@stop

@section('js')
    <script src="{{ asset('js/plugin/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('js/plugin/selectize.min.js') }}"></script>

    <script>
        $('.selectize').selectize({
            create: false,
        });

        jQuery(document).ready(function ($) {
            $('.datetimepicker').datetimepicker({
                format: 'Y-m-d H:i'
            });
            $('.datatable').DataTable();
        });

    </script>
@stop
