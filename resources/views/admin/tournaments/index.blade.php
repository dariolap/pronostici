{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('content_header')
    <h1>Admin - Tornei disponibili</h1>
@stop

@section('title', 'The Betting Game - Elenco Tornei')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">

        <div class="card-header">
            <a href="{{ route('admin.tournaments.add') }}" class="btn btn-primary float-right">{{ __('custom.create_new') }}</a>
        </div>

        <div class="card-body">

            <table class="table datatable responsive">
                <thead class="no-border">
                <tr>
                    <th>
                        Nome
                    </th>
                    <th>
                        Categoria
                    </th>
                    <th class="text-center">
                        Azioni
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($tournaments as $tournament)
                    <tr>
                        <td>
                            {{$tournament->name}}
                        </td>
                        <td>
                            {{$tournament->category()->first()->name}}
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.tournaments.edit', ['tournament' => $tournament->id]) }}">
                                <button type="button" class="btn btn-warning btn-xs dt-edit" style="margin-right:16px;">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>

                            <!-- Form per la cancellazione -->
                            <form action="{{ route('admin.tournaments.destroy', ['tournament' => $tournament->id]) }}" method="POST" style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-xs delete-tournament-btn" data-id="{{ $tournament->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop

@include('partials.datatable')
@section('plugins.Sweetalert2', true)
@section('js')
    <script>
        $(document).ready(function () {
            $('.delete-tournament-btn').click(function (e) {
                e.preventDefault(); // Prevent default form submission

                Swal.fire({
                    title: '{{ __('custom.are_you_sure') }}',
                    text: '{{ __('custom.cannot_undo_this_action') }}',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{ __('custom.delete') }}',
                    cancelButtonText: '{{ __('custom.no') }}',
                }).then((result) => {
                    if (result && !result.hasOwnProperty('dismiss')) {
                        $(e.target).closest('form').submit();
                    }
                });
            });
        });
    </script>
@endsection

