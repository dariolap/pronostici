@extends('page')

@section('title', 'Aggiungi news')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/plugin/summernote/summernote.css') }}">
@stop

@section('content_header')
    <h2>Admin - Aggiungi News</h2>
@stop

@section('content')
    @parent

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" enctype="multipart/form-data"
                          action="{{ route('admin.newsfeeds.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <label>Torneo</label>
                                <select name="tournament_id" class="form-control">
                                    @foreach ($tournaments as $c)
                                        <option value="{{ $c->id }}" {{ old('tournament_id') == $c->id ? 'selected': '' }}>{{ $c->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Titolo</label>
                                <input type="text" class="form-control" name="title" required>
                            </div>
                        </div>
                        <div class="row m-t">
                            <div class="col-12">
                                <label>Testo</label>
                                <textarea type="text" class="summernote" name="news" required></textarea>
                            </div>
                        </div>
                        <div class="row m-t">
                            <div class="col-xs-12 offset-md-9 col-md-3 pull-right">
                                <button type="submit" class="btn btn-md btn-primary" style="width:100%;">
                                    Pubblica feed
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="{{ asset('js/plugin/summernote/summernote.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('.summernote').summernote({
                height: 300,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });
        });
    </script>
@stop
