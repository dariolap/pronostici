{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('content_header')
    <h1>Admin - Calcola eventi</h1>
@stop

@section('content')
    @parent
    <div class="card">
        <div class="card-body">
            @if ($events->isEmpty())
                <h3>Nessun evento da calcolare</h3>
            @else
                @foreach ($events as $event)
                    <form action="{{ route('admin.postcalculate') }}" class="postcalculate-form" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                        <div class="row m-t">
                            <div class="col-xs-12 col-md-2 col-lg-2">
                                <p class="bold">{{ $event->name }}</p>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group has-feedback {{ $errors->has('result') ? 'has-error' : '' }}">
                                    <input type="text" name="result" class="form-control"
                                           value="{{ old('result') }}"
                                           placeholder="{{ trans('custom.result') }}">
                                    @if ($errors->has('result'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('result') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <x-adminlte-select2 placeholder="Marcatori" name="scorer_ids[]" id="scorer_ids_{{ $event->id }}" multiple>
                                    <x-slot name="prependSlot">
                                        <div class="input-group-text bg-gradient-info">
                                            {{ trans('custom.scorers') }}
                                        </div>
                                    </x-slot>
                                    @foreach ($event->bet as $bet)
                                        <option value="{{ $bet->id }}"
                                                {{ in_array($bet->id, old('scorer_ids', [])) ? 'selected' : '' }}>
                                            {{ trim(strtolower(str_replace('Marcatore: ', '', $bet->name))) }}
                                        </option>
                                    @endforeach
                                </x-adminlte-select2>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <div class="form-group has-feedback {{ $errors->has('final_summary') ? 'has-error' : '' }}">
                                    <input type="text" name="final_summary" class="form-control"
                                           value="{{ old('final_summary') }}"
                                           placeholder="{{ trans('custom.final_summary') }}">
                                    @if ($errors->has('final_summary'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('final_summary') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <button type="submit"
                                        class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                            </div>

                        </div>
                    </form>
                @endforeach
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            @if ($events->isEmpty())
                <h3>Nessun evento da annullare</h3>
            @else
                @foreach ($calculated as $event)
                    <form action="{{ route('admin.undocalculate') }}" class="undocalculate-form" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="event_id" value="{{ $event->id }}">
                        <div class="row m-t">
                            <div class="col-xs-12 col-md-11">
                                <p class="bold">{{ $event->name }}</p>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <button type="submit"
                                        class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.cancel_calculation') }}</button>
                            </div>

                        </div>
                    </form>
                @endforeach
            @endif
        </div>
    </div>



    @endif

@stop

@section('js')
@section('plugins.Select2', true)
    <script src="{{ asset('js/plugin/selectize.min.js') }}"></script>
<script>
        $(document).find('form.postcalculate-form').on('submit', function (e) {
            e.preventDefault()

            var form = $(this);

            $.ajax({
                method: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (data) {
                    if (data.status && data.status == 'success') {
                        $(form).hide("slow", function () {
                            $(this).remove();
                            if ($('form').length <= 1) {
                                $('h1').after('<h3>Nessun evento da calcolare</h3>');
                            }

                        });
                        console.log($(form).length);
                    } else {
                        alert('Si è verificato un errore');
                        // @TODO Error
                    }
                },
            });
        });

        $(document).find('form.undocalculate-form').on('submit', function (e) {
            e.preventDefault()

            var form = $(this);

            $.ajax({
                method: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (data) {
                    if (data.status && data.status == 'success') {
                        window.reload();
                    } else {
                        alert('Si è verificato un errore');
                        // @TODO Error
                    }
                },
            });
        });


    </script>
@stop
