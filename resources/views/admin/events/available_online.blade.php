@extends('page')

@section('content_header')
    <h1>Admin - Eventi disponibili</h1>
@stop

@section('content')
    @parent

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card">
        <div class="card-body">

            @foreach($events as $event)
                <div class="row m-t">
                    <div class="col-xs-12">
                        @if ($multisport)
                            <strong>{{ $event['competition'] }}</strong> {{$event['name']}} - <a
                                    href="{{ route('admin.event.add_multisport', ['sport' => $event['sport'], 'id' => $event['key']]) }}">VAI</a>
                        @else
                            <strong>{{ $event['competition'] }}</strong> {{$event['name']}} - <a
                                    href="{{ route('admin.event.add', ['id' => $event['key']]) }}">VAI</a>
                        @endif
                    </div>
                </div>
            @endforeach

        </div>
    </div>

@stop
