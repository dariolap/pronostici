@extends('page')

@section('content')
    @parent
    <h1>Admin - Gestione albo D'oro</h1>
    <div class="card">
        <div class="card-header">
            <span>{{ __('custom.hall_of_fame') }}</span>
            <a href="{{ route('admin.hall_of_fames.create') }}" class="btn btn-primary float-right">{{ __('custom.create_new') }}</a>
        </div>

        <div class="card-body">

            <table class="table">
                <thead>
                <tr>
                    <th scope="col">{{ __('hall_of_fame.id') }}</th>
                    <th scope="col">{{ __('hall_of_fame.tournament') }}</th>
                    <th scope="col">{{ __('hall_of_fame.first_users') }}</th>
                    <th scope="col">{{ __('hall_of_fame.second_users') }}</th>
                    <th scope="col">{{ __('hall_of_fame.third_users') }}</th>
                    <th scope="col">{{ __('hall_of_fame.first_points') }}</th>
                    <th scope="col">{{ __('hall_of_fame.total_points_first') }}</th>
                    <th scope="col">{{ __('hall_of_fame.second_points') }}</th>
                    <th scope="col">{{ __('hall_of_fame.total_points_second') }}</th>
                    <th scope="col">{{ __('hall_of_fame.third_points') }}</th>
                    <th scope="col">{{ __('hall_of_fame.total_points_third') }}</th>
                    <th scope="col">{{ __('hall_of_fame.best_odd_win_users') }}</th>
                    <th scope="col">{{ __('hall_of_fame.best_odd_win') }}</th>
                    <th scope="col">{{ __('hall_of_fame.high_prediction_win_users') }}</th>
                    <th scope="col">{{ __('hall_of_fame.total_high_prediction_win') }}</th>
                    <th scope="col">{{ __('hall_of_fame.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($hallOfFames as $hallOfFame)
                    <tr>
                        <th>{{ $hallOfFame->id }}</th>
                        <td>{{ $hallOfFame->tournament->name }}</td>
                        <td>
                            @foreach ($hallOfFame->firstUsers as $user)
                                {{ $user['name'] }},
                            @endforeach
                        </td>
                        <td>
                            @foreach ($hallOfFame->secondUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            @foreach ($hallOfFame->thirdUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            @foreach ($hallOfFame->firstPointsUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_points_first, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->secondPointsUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_points_second, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->thirdPointsUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_points_third, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->bestOddWinUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->best_odd_win, 2) }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->highPredictionWinUsers()->pluck('name') as $user)
                                {{ $user }},
                            @endforeach
                        </td>
                        <td>
                            {{ number_format($hallOfFame->total_high_prediction_win, 2) }}
                        </td>

                        <td>
                            <a href="{{ route('admin.hall_of_fames.edit', $hallOfFame->id) }}"
                               class="btn btn-secondary">{{ __('hall_of_fame.Edit') }}</a>
                            <form action="{{ route('admin.hall_of_fames.destroy', $hallOfFame->id) }}" method="POST"
                                  style="display: inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">{{ __('hall_of_fame.Delete') }}</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
