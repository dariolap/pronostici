@extends('page')

@section('css')
    @parent
@stop

@section('content_header')
    <h1>{{ __('hall_of_fame.admin_edit_hall_of_fame') }}</h1>
@stop

@section('content')
    @parent

    <div class="card">
        <div class="card-body">

            @include('partials.messages')

            <form action="{{ route('admin.hall_of_fames.update', $hallOfFame->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="form-row">
                    <div class="form-group col-sm-12">
                        <label for="tournament_id">{{ __('hall_of_fame.tournament') }}</label>
                        <x-adminlte-select2 name="tournament_id" id="tournament_id"
                                            class="selectize">
                            @foreach ($tournaments as $tournament)
                                <option value="{{ $tournament->id }}" {{ old('tournament_id', $hallOfFame->tournament_id) == $tournament->id ? 'selected' : '' }}>
                                    {{ $tournament->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-4">
                        <label for="first_user_ids">{{ __('hall_of_fame.first_users') }}</label>
                        <x-adminlte-select2 name="first_user_ids[]" id="first_user_ids" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('first_user_ids', $hallOfFame->first_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-4">
                        <label for="second_user_ids">{{ __('hall_of_fame.second_users') }}</label>
                        <x-adminlte-select2 name="second_user_ids[]" id="second_user_ids" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('second_user_ids', $hallOfFame->second_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-4">
                        <label for="third_user_ids">{{ __('hall_of_fame.third_users') }}</label>
                        <x-adminlte-select2 name="third_user_ids[]" id="third_user_ids" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('third_user_ids', $hallOfFame->third_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="first_points_user_ids">{{ __('hall_of_fame.first_points') }}</label>
                        <x-adminlte-select2 name="first_points_user_ids[]" id="first_points_user_ids" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('first_points_user_ids', $hallOfFame->first_points_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="total_points_first">{{ __('hall_of_fame.total_points_first') }}</label>
                        <input type="number" name="total_points_first" id="total_points_first" class="form-control"
                               step="0.01" value="{{ old('total_points_first', $hallOfFame->total_points_first) }}">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="second_points_user_ids">{{ __('hall_of_fame.second_points') }}</label>
                        <x-adminlte-select2 name="second_points_user_ids[]" id="second_points_user_ids" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('second_points_user_ids', $hallOfFame->second_points_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="total_points_second">{{ __('hall_of_fame.total_points_second') }}</label>
                        <input type="number" name="total_points_second" id="total_points_second" class="form-control"
                               step="0.01" value="{{ old('total_points_second', $hallOfFame->total_points_second) }}">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="third_user_ids">{{ __('hall_of_fame.third_points') }}</label>
                        <x-adminlte-select2 name="third_points_user_ids[]" id="third_points_user_ids" multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('third_points_user_ids', $hallOfFame->third_points_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="total_points_third">{{ __('hall_of_fame.total_points_third') }}</label>
                        <input type="number" name="total_points_third" id="total_points_third" class="form-control"
                               step="0.01" value="{{ old('total_points_third', $hallOfFame->total_points_third) }}">
                    </div>

                    <div class="form-group col-12 highest_odd_win_wrapper">
                        <div class="highest_odd_win">
                        @if($hallOfFame->best_odd_wins)
                            @foreach($hallOfFame->best_odd_wins as $key => $value)
                                <!--                            TODO: GESTIRE OLD-->
                                    @include('admin.hall_of_fame.partials.highest_odd_win_row', ['key' => $key, 'value' => $value])
                                @endforeach
                            @else
                                @include('admin.hall_of_fame.partials.highest_odd_win_row', ['key' => 0, 'value' => null])
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-12">
                        <label for="total_best_odd_win">{{ __('hall_of_fame.total_best_odd_win') }}</label>
                        <input type="number" name="total_best_odd_win" id="total_best_odd_win"
                               class="form-control" step="0.01"
                               value="{{ old('total_best_odd_win', $hallOfFame->total_best_odd_win) }}">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="high_prediction_win_user_ids">{{ __('hall_of_fame.high_prediction_win_users') }}</label>
                        <x-adminlte-select2 name="high_prediction_win_user_ids[]" id="best_odd_win_user_ids"
                                            class="selectize"
                                            multiple>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ in_array($user->id, old('high_prediction_win_user_ids', $hallOfFame->high_prediction_win_user_ids ?? [])) ? 'selected' : '' }}>
                                    {{ $user->name }}
                                </option>
                            @endforeach
                        </x-adminlte-select2>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="total_high_prediction_win">{{ __('hall_of_fame.total_high_prediction_win') }}</label>
                        <input type="number" name="total_high_prediction_win" id="total_high_prediction_win"
                               class="form-control" step="0.01"
                               value="{{ old('total_high_prediction_win', $hallOfFame->total_high_prediction_win) }}">
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit"
                            class="btn btn-primary">{{ __('custom.edit') }}</button>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('plugins.Select2', true)
@section('js')
    <script>
        //Initialize Select2 Elements
        $('select').select2({
            theme: 'bootstrap4'
        });
    </script>
@endsection
