@extends('page')

@section('content_header')
    <h1>Admin - Aggiungi Fase</h1>
@stop

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/plugin/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/plugin/selectize/selectize.css') }}">
@stop

@section('title', 'The Betting Game - Aggiungi Fase')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

        <form action="{{ url(route('admin.phases.store')) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="tournament_id">{{ trans('custom.tournament') }}</label>
                            <select name="tournament_id" id="tournament_id"
                                    class="selectize {{ $errors->has('tournament_id') ? 'is-invalid' : '' }}">
                                <option value="">{{ trans('custom.select') }}</option>
                                @foreach ($tournaments as $tournament)
                                    <option value="{{ $tournament->id }}" {{ old('tournament_id') == $tournament->id ? 'selected' : '' }}>
                                        {{ $tournament->name }}
                                    </option>
                                @endforeach
                            </select>
                            @if ($errors->has('tournament_id'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('tournament_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="phase">{{ trans('custom.number') }}</label>
                            <input type="number" name="phase"
                                   class="form-control {{ $errors->has('phase') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.number') }}"
                                   value="{{ old('phase') }}">
                            @if ($errors->has('phase'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('phase') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="name">{{ trans('custom.name') }}</label>
                            <input type="text" name="name"
                                   class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.name') }}"
                                   value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="type_id">{{ trans('custom.type') }}</label>
                            <select name="type_id" id="type"
                                    class="selectize {{ $errors->has('type_id') ? 'is-invalid' : '' }}">
                                <option value="">{{ trans('custom.select') }}</option>
                                @foreach ($types as $type)
                                    <option value="{{ $type->id }}" {{ old('type_id') == $type->id ? 'selected' : '' }}>
                                        {{ ucfirst($type->name) }}
                                    </option>
                                @endforeach
                            </select>
                            @if ($errors->has('type_id'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('type_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="n_qualified">{{ trans('custom.n_qualified') }}</label>
                            <input type="number" name="n_qualified"
                                   class="form-control {{ $errors->has('n_qualified') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.n_qualified') }}"
                                   value="{{ old('n_qualified') }}">
                            @if ($errors->has('n_qualified'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('n_qualified') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="n_playoff">{{ trans('custom.n_playoff') }}</label>
                            <input type="number" name="n_playoff"
                                   class="form-control {{ $errors->has('n_playoff') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.n_playoff') }}"
                                   value="{{ old('n_playoff') }}">
                            @if ($errors->has('n_playoff'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('n_playoff') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="start_date">{{ trans('custom.start_date') }}</label>
                            <input type="text" name="start_date"
                                   class="form-control datetimepicker {{ $errors->has('start_date') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.start_date') }}"
                                   value="{{ old('start_date') }}">
                            @if ($errors->has('start_date'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('start_date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group has-feedback">
                            <label for="end_date">{{ trans('custom.end_date') }}</label>
                            <input type="text" name="end_date"
                                   class="form-control datetimepicker {{ $errors->has('end_date') ? 'is-invalid' : '' }}"
                                   placeholder="{{ trans('custom.end_date') }}"
                                   value="{{ old('end_date') }}">
                            @if ($errors->has('end_date'))
                                <span class="error invalid-feedback">
                                    <strong>{{ $errors->first('end_date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 offset-md-9 col-xs-12 pull-right m-t">
                        <button type="submit"
                                class="form-control btn btn-primary btn-block btn-flat">{{ trans('custom.save') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

@stop

@section('js')
    <script src="{{ asset('js/plugin/selectize.min.js') }}"></script>
    <script src="{{ asset('js/plugin/jquery.datetimepicker.full.min.js') }}"></script>
    <script>
        $('.selectize').selectize({
            create: false,
        });

        jQuery(document).ready(function ($) {
            $('.datetimepicker').datetimepicker({
                format: 'Y-m-d H:i'
            });
        });
    </script>
@stop
