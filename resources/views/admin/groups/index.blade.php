@extends('page')

@section('content_header')
    <h1>Admin - Lista gruppi</h1>
@stop

@section('title', 'The Betting Game - Elenco Gruppi')

@section('content')
    @parent

    @include('partials.messages')

    <div class="card">

        <div class="card-header">
            <a href="{{ route('admin.groups.add') }}" class="btn btn-primary float-right">{{ __('custom.create_new') }}</a>
        </div>

        <div class="card-body">

            <table class="datatable responsive">
                <thead>
                <th>
                    Torneo
                </th>
                <th>
                    Fase
                </th>
                <th>
                    Bonus
                </th>
                <th>
                    Gruppo
                </th>
                <th>
                    Utenti
                </th>
                <th class="text-center">
                    Azioni
                </th>

                </thead>
                <tbody>
                @foreach($groups as $group)
                    <tr>
                        <td>
                            {{$group->phase->tournament->name}}
                        </td>
                        <td>
                            {{$group->phase->name}}
                        </td>
                        <td>
                            {{$group->bonus}}
                        </td>
                        <td>
                            {{$group->group}}
                        </td>
                        <td>
                            @foreach($group->users as $user)
                                {{$user->name}}<br>
                            @endforeach
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.groups.edit', ['group' => $group->id]) }}">
                                <button type="button" class="btn btn-warning btn-xs dt-edit"
                                        style="margin-right:16px;">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>
                            <!-- Form per la cancellazione -->
                            <form action="{{ route('admin.groups.destroy', ['group' => $group->id]) }}" method="POST"
                                  style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-xs delete-group-btn"
                                        data-id="{{ $group->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
@include('partials.datatable')
@section('js')
@section('plugins.Sweetalert2', true)

<script>
    $(document).ready(function () {
        $('.delete-group-btn').click(function (e) {
            e.preventDefault(); // Prevent default form submission

            Swal.fire({
                title: '{{ __('custom.are_you_sure') }}',
                text: '{{ __('custom.cannot_undo_this_action') }}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __('custom.delete') }}',
                cancelButtonText: '{{ __('custom.no') }}',
            }).then((result) => {
                console.log(result);
                if (result && !result.hasOwnProperty('dismiss')) {
                    $(e.target).closest('form').submit();
                }
            });
        });
    });
</script>
@endsection

