<div class="col-12 mt-10">
    <p>- Per giocare, una volta iscritti al sito, bisogna, dopo aver fatto login, aggiungersi al torneo in corso.</p>

    <p>- Il gioco è con le quote dei bookmaker (in questo caso betclic): si punta su uno degli esiti
        disponibili per ogni evento e, in caso di risultato indovinato, si riceve un punteggio pari al
        valore della quota puntata.
    </p>

    <div>- Per un singolo evento sarà possibile scegliere una sola di questa tipologia di pronostici:
        <ul class="rules">
            <li>Risultato finale (1-X-2)</li>
            <li>Doppia Chance (1X - X2 - 12)</li>
            <li>Under 2.5 (il totale dei gol segnati nel match sarà minore di 2.5)</li>
            <li>Over 2.5 (il totale dei gol segnati nel match sarà maggiore di 2.5)</li>
            <li>Goal (Entrambe le squadre andranno a segno)</li>
            <li>No Goal (Una delle due squadre non andrà a segno)</li>
            <li>Giocatore a segno nel match (in qualunque momento nei tempi regolamentari)</li>
            <li>Risultato esatto</li>
        </ul>
        <div>Il pronostico è da considerarsi nei 90 minuti della partita (quindi senza contare eventuali
            supplementari e rigori).
        </div>
    </div>

    <div>- Il punteggio totale del partecipante sarà la somma delle quote indovinate.</div>

    <div>- Un evento non indovinato o non pronosticato varrà 0 punti.</div>

    <div>- Si può modificare ogni pronostico quante si vuole entro l'orario per cui è fissato l'orario di inizio
        dell'evento.
    </div>

    <div>- In caso di eventi rinviati, se queste sono già iniziate non sarà possibile modificare il pronostico,
        mentre se questi non hanno avuto inizio la scadenza sarà slittata alla nuova data.<br>
        In caso di match annullati non si assegneranno punti.
    </div>

    <div>- I match da pronosticare sono generalmente disponibili entro 24 ore dall'inizio della giornata e i conteggi,
        salvo problemi, eseguiti entro 24 ore dopo il termine dell'evento.
    </div>

    <p>Il gioco, l'iscrizione e i tornei sono totalmente gratuiti.</p>
</div>
