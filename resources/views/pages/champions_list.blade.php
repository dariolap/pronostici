@extends('page')

@section('content_header')
    <h1>{{ __('custom.champions_list') }}</h1>
@stop

@section('content')
    @parent

    <div class="card champions_list">
        <table class="datatable responsive table-layout-fixed stripe">
            <thead class="text-primary">
            <tr>
                <th>&nbsp;</th>
                <th>
                    <strong>Vincitore Torneo</strong>
                </th>
                <th>
                    Miglior Punteggio totale
                </th>
                <th>
                    Maggior quota indovinata
                </th>
                <th>
                    Maggior numero di eventi indovinati
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-success" scope="row"><strong>Olimpiadi Parigi 2024</strong> (solo classifica generale)
                </td>
                <td class="border-right-0"><strong>Oro: Mic94 (69.91)</strong><br>
                    Argento: Claudio Massarotti (59.15)<br>
                    Bronzo: Luana Russo (57.45)
                </td>
                <td class="border-right-0">/</td>
                <td>Mic94, Ange, Mike (15.00) con Pallanuoto Maschile - Vincitore Finale Serbia</td>
                <td>Claudio Massarotti, Luana Russo (22 su 47)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>{{ Collect($tournaments)->get(23)->name }}</strong> (solo
                    classifica generale)
                </td>
                <td class="border-right-0"><strong>Mic94 (41.89)</strong></td>
                <td class="border-right-0">/</td>
                <td>Sealo33: 14 con Bolivia - Panama Risultato esatto: 1-3</td>
                <td>Mic94 (19)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>{{ Collect($tournaments)->get(22)->name }}</strong></td>
                <td class="border-right-0"><strong>Paolo</strong></td>
                <td class="border-right-0">Gozer il Gozeriano (73.70)</td>
                <td>Mattia Cattelani : 10 con Turchia - Portogallo Risultato esatto: 0-3<br>
                    Mic94 e Paolo: 10 con Olanda - Inghilterra Risultato esatto: 1-2<br>
                    Vincent Jordan, Daniele Sangermano e Mirko P.: 10 con Spagna - Francia Risultato esatto: 2-1
                </td>
                <td>Paolo (31)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>{{ Collect($tournaments)->get(19)->name }}</strong></td>
                <td class="border-right-0"><strong>Francesco 🇫🇴</strong></td>
                <td class="border-right-0">Alessandro Zincone (154.01)</td>
                <td>Alessandro Zincone: 60 con Benfica - Inter Risultato esatto: 3-3</td>
                <td>Mike (80)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>{{ Collect($tournaments)->get(21)->name }}</strong></td>
                <td class="border-right-0"><strong>Liezmig</strong></td>
                <td class="border-right-0">luigi campese (77.34)</td>
                <td>Alessandro Zincone: 50 con Milan - Slavia Praga Risultato esatto: 4-2</td>
                <td>luigi campese (32)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>{{ Collect($tournaments)->get(20)->name }}</strong> (solo
                    classifica generale)
                </td>
                <td class="border-right-0"><strong>Ange (77.62)</strong></td>
                <td class="border-right-0">/</td>
                <td>Francesco 🇫🇴: 28 con RD Congo - Guinea Risultato esatto: 3-1</td>
                <td>Ale R e AleGM93 (28)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Euro Under 21 2023</strong> (solo classifica generale)</td>
                <td><strong>AleGM93 (47.14)</strong></td>
                <td>/</td>
                <td>Alessandro Zincone (30) con Svizzera - Italia Risultato esatto: 2-3
                </td>
                <td>AleGM93 (22)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Champions League 2022/23</strong></td>
                <td><strong>Daniele Sangermano</strong></td>
                <td>Simone (150.15)</td>
                <td>Alessandro Zincone (70.00) con Inter - Benfica Risultato esatto: 3-3
                </td>
                <td>Hermann Thierry Marquiand (85)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Europa League 2023</strong></td>
                <td><strong>Massum</strong></td>
                <td>Massum (58.62)</td>
                <td>Daniele Sangermano e Federico Ciufoli (10.00) con Friburgo - Juventus Risultato esatto: 0-2
                </td>
                <td>Claudio Massarotti (25)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Fifa World Cup Qatar 2022</strong></td>
                <td><strong>RNSupremacy</strong></td>
                <td>FraOrru (103.66)</td>
                <td>Alessandro Zincone (35.00) con Giappone - Costa Rica Marcatore: Keysher Fuller
                </td>
                <td>Hermann Thierry Marquiand (55)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Champions League 2021/22</strong></td>
                <td><strong>Dario La Padula</strong></td>
                <td>luigi campese (161.87)</td>
                <td>luigi campese (35.00) con Ajax - Sporting Lisbona Risultato esatto: 4-2
                </td>
                <td>Hermann Thierry Marquiand (81)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Europa League 2022</strong></td>
                <td><strong>Luana Russo</strong></td>
                <td>Gatto (74.95)</td>
                <td>Tommy S. (25.00) con Barcellona - Galatasaray Risultato esatto: 0-0
                </td>
                <td>Ale R & Hermann Thierry Marquiand (35)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Olimpiadi Invernali Pechino 2022</strong> (solo classifica
                    generale)
                </td>
                <td class="border-right-0"><strong>Oro: FVG (57.23)</strong><br>
                    Argento: Dario La Padula (49.30)<br>
                    Bronzo: Gozer il Gozeriano (46.45)
                </td>
                <td class="border-right-0">/</td>
                <td>Tommy S, FraOrru, Gozer il Gozeriano, FVG e Dario La Padula (25.00) con Curling Misto Italia</td>
                <td>FVG (17)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Coppa D'Africa 2021</strong> (solo classifica generale)
                </td>
                <td class="border-right-0"><strong>Dario La Padula (65.66)</strong></td>
                <td class="border-right-0">/</td>
                <td>Federico Ciufoli, Daniele Sangermano e Tommy S. (22.00) con Burkina Faso - Senegal Risultato esatto:
                    1-3
                </td>
                <td>Hermann Thierry Marquiand (44)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Olimpiadi Tokyo 2020</strong> (solo classifica generale)
                </td>
                <td class="border-right-0"><strong>Oro: Manolo (53.15)</strong><br>
                    Argento: Gatto (51.00)<br>
                    Bronzo: Massimo Palozzi (50.40)
                </td>
                <td class="border-right-0">/</td>
                <td>Gatto: 40.00 con Atletica - 100m Maschile Lamont-Marcell Jacobs
                </td>
                <td>Ale R (19)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Euro 2020</strong></td>
                <td><strong>Claudio Massarotti</strong></td>
                <td>Massum (67.57)</td>
                <td>luigi campese (12.00) con Belgio - Russia Risultato esatto: 3-0
                </td>
                <td>Luca Pacella (31)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Euro Under 21 2021</strong> (solo classifica generale)</td>
                <td><strong>Ale R (90)</strong></td>
                <td>/</td>
                <td>Ale R (90.00) con Portogallo U21 - Italia U21 Risultato esatto: 3-3
                </td>
                <td>AleGM93 (5)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Champions League 2020/21</strong></td>
                <td><strong>Fabio Prencipe</strong></td>
                <td>FraOrru (143.75)</td>
                <td>FraOrru (70.00) con Siviglia - Chelsea Risultato esatto: 0-4<br>
                    Tommy S. (70.00) con Barcelona - Paris Saint-Germain Risultato esatto: 1-4
                </td>
                <td>Dario La Padula (83)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Europa League 2021</strong></td>
                <td><strong>Danilo C.</strong></td>
                <td>Fabrizio Di Coste (79.52)</td>
                <td>Fabrizio Di Coste (35.00) con Ajax - Roma Marcatore: Ibañez
                </td>
                <td>FVG (36)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Champions League 2019/20</strong></td>
                <td><strong>AleGM93</strong></td>
                <td>Gozer il Gozeriano (147.14)</td>
                <td>Gozer il Gozeriano (45.00) con Tottenham - Olympiacos Risultato esatto: 4-2</td>
                <td>Davide S. (99)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Europa League 2020</strong></td>
                <td><strong>Mirko P.</strong></td>
                <td>Fabrizio Di Coste (72.27)</td>
                <td>Luigi campese (19.00) con Manchester United - Club Brugge Risultato esatto: Altro<br>
                    Fabrizio Di Coste (19.00) con Siviglia - Inter Marcatore: Diego Carlos
                </td>
                <td>Davide S. (39)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Copa America 2019</strong> (solo classifica generale)
                </td>
                <td class="border-right-0"><strong>Alessandro (29.29)</strong></td>
                <td class="border-right-0">/</td>
                <td>Marta: (13.00) con Bolivia - Venezuela Risultato esatto: 1-3<br>
                    Dario La Padula, Jacopo Guastella e Danilo C. (13.00) con Brasile - Perù Risultato esatto: 3-1<br>
                </td>
                <td>Davide S. (21)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Champions League 2018/19</strong></td>
                <td><strong>Mattew</strong></td>
                <td>Gastone (145.53)</td>
                <td>Gastone (19.00) con Liverpool - Paris Saint-Germain Risultato esatto: 3-2</td>
                <td>Francesco Orga (84)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>Europa League 2019</strong></td>
                <td><strong>Ale R</strong></td>
                <td>Alessandro (68.36)</td>
                <td>Salvatore lavorgna (18.00) con Zenit San-Pietroburgo - Fenerbahce Risultato esatto: 3-1</td>
                <td>Massimo Orsini (41)</td>
            </tr>
            <tr>
                <td class="text-success" scope="row"><strong>{{ __('tournaments.fifa_world_cup') }} 2018</strong>
                </td>
                <td><strong>Gozer il Gozeriano</strong></td>
                <td>Gozer il Gozeriano (96.82)</td>
                <td>Gozer il Gozeriano (21.00) con Croazia - Inghilterra Marcatore: Kieran Trippier</td>
                <td>Ludovica Ciufoli (39)</td>
            </tr>
            </tbody>
        </table>
    </div>
@stop

@include('partials.datatable')

