<?php

use Illuminate\Support\Carbon ?>

{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('content_header')
    @if ($user->id == auth()->user()->id)
        <h1>{{ config('app.name') }} - I miei pronostici - {{ $tournament->name }}</h1>
    @else
        <h1>{{ config('app.name') }} - {{ $user->name }} - {{ $tournament->name }}</h1>
    @endif
@stop

@section('content')
    @parent

    <div class="card">
            <table class="datatable responsive stripe">
                @php
                    $win = 0;
                    $total = 0;
                @endphp
                <thead class="text-center">
                    <th data-priority="3">
                        Data
                    </th>
                    <th data-priority="1">
                        Evento
                    </th>
                    <th data-priority="1">
                        Pronostico
                    </th>
                    <th data-priority="1">
                        Quota
                    </th>
                    <th data-priority="2">
                        Visualizza
                    </th>
                </thead>
                <tbody class="text-center">
                @foreach ($results as $result)
                    <tr>
                        <td>{{ Carbon::parse($result->deadline)->format('d/m/Y H:i') }}</td>
                        <td class="event-name">{{ $result->event }}</td>
                        </td>
                        @if(Carbon::parse($result->deadline)->isFuture() && $result->private && $user->id != Auth::user()->id)
                            <td>
                                {{ strtoupper(__('custom.private_bet')) }}
                            </td>
                            <td>
                                {{ strtoupper(__('custom.private_bet')) }}
                            </td>
                        @else
                            <td>

                                @if ($result->win < 0)
                                    <del class="text-danger">{{ $result->bet }}</del>
                                @elseif ($result->win > 0)
                                    @php
                                        $total += $result->points;
                                        $win += 1;
                                    @endphp
                                    <span class="text-success">{{ $result->bet }}</span>
                                @else
                                    {{ $result->bet }}
                                @endif
                            </td>
                            <td>@if ($result->win < 0)
                                    <del class="text-danger">{{ number_format($result->points,2) }}</del>
                                @elseif ($result->win > 0)
                                    <span class="text-success bold text-14">{{ number_format($result->points, 2) }}</span>
                                @else
                                    {{ number_format($result->points, 2) }}
                                @endif
                            </td>
                        @endif
                        <td>
                            <a class="event pointer" data-event_id="{{ $result->event_id }}"
                               data-tournament_id="{{ $tournament->id }}">
                                <button class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="p-2">
                <p class="bold text-22"><strong>Punteggio totale</strong>: {{ $total }}</p>
                <p class="bold text-22"><strong>Pronostici indovinati</strong>: {{ $win }}</p>
            </div>
    </div>

@stop

@include('partials.datatable')
