<?php
use Illuminate\Support\Carbon ?>

{{ number_format(Collect($quarter[$id])->filter(function ($item, $key) {
    return Carbon::parse($item['deadline'])->gte(Carbon::parse('2018-07-14 00:00:00')) &&
        Carbon::parse($item['deadline'])->lte(Carbon::parse('2018-07-15 23:00:00'));
    })->sum('points'), 2)
}}