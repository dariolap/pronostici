{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('title', 'The Bettinga Game - Mondiali 2018')

@section('content')
@parent
<h1>Pronostici Mondiali - Aggiornamenti</h1>

<div class="text-16">

    <h3>
        <strong>15 Luglio</strong>
    </h3>
	Siamo ai saluti. Innanzitutto grazie a tutti coloro che hanno giocato fino alla fine e per le belle parole su "Il Bar". Gioco andato meglio delle più rosee previsioni.<br>
	Un grande <strong>Gozer il Gozeriano</strong> si prende il primo premio, il miglior punteggio e la quota più alta giocata. Dato che, come preannunciato ad inizio gioco, il premio era la "gloria", abbiamo provveduto a consegnargli una foto di Gloria Guida. Questo è il massimo di premio che potevamo permetterci visto le disponibilità economiche, anzi nemmeno l'ho pagato io visto che il foglio e il toner erano di Ciufoli.
	<img src="{{ asset('img/roberto1.jpg') }}" alt="Panama" class="img-responsive" style="margin-left: auto; margin-right: auto"/>
	<img src="{{ asset('img/roberto2.jpg') }}" alt="Panama" class="img-responsive" style="margin-left: auto; margin-right: auto"/>
    <br>
    Una stagione perfetta rovinata solo in questi ultimi giorni. Mirko questa volta deve accontentarsi del secondo gradino del podio. Siamo in attesa di sue foto ma il segnale con Focene è interrotto.<br>
    <br>
    Si sono sfidate per il secondo miglior punteggio, terzo posto e premio femminile <strong>Manolo</strong> e <strong>Ludovica</strong>. Un match molto MASCHIO (come si vede in foto) che vede la Ciufoli trionfatrice in tutte e 3 i casi e diventa anche colei con il maggior numero di giocate indovinate (39 su 64). Eccole amiche come una volta in esclusiva dopo il grande match.
	<img src="{{ asset('img/manu_ludo.jpg') }}" alt="Panama" class="img-responsive" style="margin-left: auto; margin-right: auto"/>
    <br>
    Adesso un po' di relax dopo un mese pieno. Per il resto, avrete presto mie notizie...


     <h3>
        <strong>14 Luglio</strong>
    </h3>

    Finale a reti bianche dopo il primo match. Ludovica passa in vantaggio nella finalina. Oltre lei solo Emanuele va a segno

     <h3>
        <strong>11 Luglio</strong>
    </h3>
	Ieri è stata fatta un'impresa e già quasi celebravamo Manolo in finale. Oggi però è stata scritta la storia: marcatore Trippier e Gozer vola in finale, in fuga nel miglior punteggio e si prende in solitaria il primato per la quota più alta giocata (già suo ma in condivisione con altre due persone). Un eroe.<br>
    Ci dispiace per Manolo che non andrà in finale nonostante sia stata l'unica ad azzeccare un risultato esatto in questi due giorni. Grazie ad entrambi per averci regalato la nuova partita del secolo.<br>
    In finale ci sarà Mirko, che anche questa volta passa con il minimo indispensabile.<br>
    Gozer strafavorito? assolutamente no, abbiamo già visto che Mirko è abile a rincretinire gli avversari.<br>
    Ludovica se la giocherà comunque con Manolo per il miglior punteggio femminile e con Salvatore per il premio di maggior numero di eventi presi.<br>



    <h3>
        <strong>10 Luglio</strong>
    </h3>
    Non volevo scrivere, ma dato che ha preso l'1-0: cara Manolo, quando ci apriamo una bella società di scommesse?<br>
    Gozer, ora serve il colpo da maestro, sempre che la tua dolce metà non faccia un altro miracolo.<br>
    Mirko e Ludovica sbagliano entrambi. in caso di 0-0 passerà chi ha il miglior punteggio del turno precedente, quindi Mirko potrà commettere ulteriori errori.<br>
    In arrivo una clamorosa finale fra due donne che non sanno nemmeno nominarci 3-4 giocatori di chi sta giocando queste semifinali?<br>
    Solo in 4 indovinano il match di oggi, tra cui una Alessandra Medda che non molla niente fino alla fine. Da esempio.<br>
    Segnaliamo un Frizziero che poteva lottare al miglior punteggio e invece per lui finisce il mondiale e inizia Temptation Island. Delusione<br>
    Ci abbandonano anche Fabrizio DC e Alessandro Zaffini, dove siete finiti? questo gioco senza di voi perde di qualità.<br>

    <h3>
        <strong>10 Luglio</strong>
    </h3>
    Il giorno delle semifinali è arrivato.<br>
    Piccole novità:<br>
    -Ai normali pronostici che avete visto finora, aggiunti anche risultato esatto e marcatore (per la felicità mia che ho trovato il modo di importarli in automatico e di chi me lo aveva chiesto)<br>
    -Votate nel sondaggio (anche perchè se no non vi fa inserire i pronostici)<br>
    <br>
    Siccome non ho ancora testato il calcolo automatico con i marcatori e i risultati esatti, i conteggi potrebbero subire alcuni minuti di ritardo
    <h3>
        <strong>7 Luglio</strong>
    </h3>
    <br>
    In un match in cui ci si giocava, oltre al passaggio in semifinale, anche il primato in classifica generale, Gozer ne prende 4 su 4 e sconfigge Massimo, che nonostante il terzo punteggio, saluta il gioco condannato da un sorteggio non favolevole.<br>
    Manolo ha vita facile contro un Salvatore non apparso in forma dopo le imprese al girone e agli ottavi.<br>
    Alessandro rischia più del previsto ma la sorte non lo aiuta. Ne approfitta Ludovica che passa con il minimo sforzo.<br>
    Disastro Frizziero che gioca improbabili over Svedesi o No Goal Belga. Si rifà con l'X della Russia ma ormai è troppo tardi e serve solo a portarsi al secondo posto in classifica generale. A Mirko bastano 3.26 punti e il goal brasiliano per passare il turno.<br>
    <br>
    <strong>Semifinali</strong><br>
    In semifinale giunge gente che dubito abbia seguito una partita intera in tutta la competizione o che braciolava a tv spenta durante Brasile - Belgio. Ma facciamo finta di nulla e andiamo avanti.<br>
    Per tutto il torneo ci siamo augurati derby familiari e quale migliore momento se non quello delle semifinali per vedere una guerra sotto lo stesso tetto? Tra Gozer e Manolo è sfida tra marito e moglie e siamo sicuri che in casa Abate/Rettig i nervi saranno tesi nelle serate di Martedì e Mercoledì. Ma ora spazio alla festa, per una famiglia il cui nome è stato sempre infangato dagli insuccessi del fratello/cognato Alessandro.<br>
    Mirko, unico intruso in un torneo che per ora parla "Terre Parsiche", dopo aver vinto 634 trofei nel corso della stagione, continua a dire di essere sfigato, che l'avversario ha già vinto e intanto si prepara a vincere anche questo gioco e il fantamondiale per essere l'uomo più vincente della storia in una sola stagione. Ludovica, dopo aver sconfitto probabilmente il più esperto rimasto in gioco, vuole la finale tutta femminile contro Manolo, per dimostrare che a questo gioco conta solo la fortuna, e poi prenotarsi la vacanza in Croazia per festeggiare con loro.
    <br>

    <h3>
        <strong>3 Luglio</strong>
    </h3>
    Si concludono gli ottavi con molte eliminazioni celebri e outsider incredibili.<br>
    <br>
    Gozer aveva un vantaggio troppo grande e a Fabio non è riuscita l'impresa impossibile. <br>
    Gestiscono bene il vantaggio anche Massimo, Ludovica, Mirko e Andrea.<br>
    Le uniche rimonte riescono a Salvatore, che manda a casa uno dei favoriti, il Sir Di Coste che finora mai nessuno aveva sconfitto in uno scontro diretto in un gioco a quote, e ad Alessandro. Quest'ultimo ringrazia Forsberg e supera di soli 30 centesimi uno sportivissimo Jacopo che gli rilascia pubblici complimenti.<br>
    Deve sudare fino all'ultimo minuto dell'ultima partita Manolo, che in vantaggio di soli 6 centesimi prima del match finale, rischia la beffa fino all'ultimo contro Enrico che aveva l'over di Colombia - Inghilterra. Con questo risultato raggiunto Rettig sister potrà dire di aver fatto meglio lei in questo gioco che il fratello nella sua vita di fantacalci. Onore femminile e fratello a casa tenuto alto anche da Ludovica.<br>
    <br>
    <strong>Quarti</strong><br>
    Il calendario ci regala subito la sfida tra il primo e il terzo della classifica generale, Gozer e Massimo.<br>
    Contro anche le due rivelazioni del gioco, Manolo e Salvatore.<br>
    Esame duro anche per Ludovica che dovrà affrontare Alessandro, uno dei maggiori esperti calcistici rimasti in gioco, autore di due rimonte al girone e agli ottavi.<br>
    Mirko si conferma ancora una volta il migliore del gruppo Mariotto/Falafel, rimanendo ancora in gioco quando Danilo, Dario, Enrico e Fabrizio stanno a casa a guardare. Ma a guardarlo c'è anche il suo amico Davide s con la banda del fantamondiale
    <br>
    <strong>LE PARTITE DEI QUARTI DI FINALE INIZIERÒ A INSERIRLE DOMANI</strong>
    <h3>
        <strong>28 Giugno</strong>
    </h3>
    Si conclude la prima fase, alcune eliminazioni incredibili e inaspettate.<br>
    <br>
    <strong>Gruppo A:</strong> a sorpresa fuori uno dei mostri sacri Danilo. Nell'ultima drammatica giornata è stata lotta a 4 per due posti tra Alessandro, Dario, Danilo e Mattew. Bravo Alessandro a rischiare e balzare dal sesto al terzo posto. Gozer e Manolo, al momento miglior coppia del gioco, se ne vanno a braccetto agli ottavi. Altra delusione del girone il giovane (probabilmente il più piccolo del gioco) ma esperto Gianluca che chiude ultimo.<br>
    <strong>Gruppo B:</strong> nessun ribaltone in questa ultima giornata: Mirko ed Enrico avevano già accumulato un buon vantaggio e il 15 preso da Jacopo ieri lo ha portato in zona tranquilla. Fabio respinge gli attacchi e dovrà annullare il biglietto di ritorno, già prenotato prima della clamorosa giornata di ieri<br>
    <strong>Gruppo C:</strong> anche qui fuori un big. Riccardo T, sempre messo bene in questo girone, abbandona le posizioni che contano proprio alla 48esima partita. Davide M ha alzato bandiera bianca in queste ultime due giornate, mentre un grande Salvatore mette la zampata vincente e chiude al terzo posto.<br>
    <strong>Gruppo D:</strong> in un girone che ogni giorno cambiava le gerarchie, la spunta a sorpresa Massimo. Fuori lo spregiudicato Ale R, che ha preso giocate mostruose ma spesso rischiato troppo, ed ora se ne torna a casa sfottuto dalla sorella ancora fortemente in gioco. Saluta anche un big come Gastone a cui non riesce il tris fantacalcio-colleghi, fantagallo e mondiale.<br>
    <br>
    <strong>Ottavi</strong><br>
    <br>
    Innanzitutto sfuma il derby dei Ciufoli per cui anche Mediaset si era interessato all'acquisto dei diritti. Anche altri due conoscenti come Frizziero - Rastelli sarebbe stato molto interessante averli di fronte.<br>
    Nel match di coloro che stanno ancora festeggiando l'1 della Corea del Sud, tra Gozer e Fabio c'è un abisso nel punteggio. Riuscirà quest'ultimo nel miracolo?<br>
    Buon vantaggio anche per Massimo contro Davide. La sorpresa continua?<br>
    Manolo, dimostrato che anche lei può umiliare tranquillamente il fratello in ambito calcistico, sarà ora chiamata alla prova di maturità contro un leone come Enrico. 3.50 punti di vantaggio basteranno?<br>
    Il giovane contro l'esperto: riuscirà Salvatore, che al momento è colui che ha indovinato più pronostici (e pensare che alcune partite le ha saltate) a ribaltare il pronostico e far fuori il vincitore della Champions League?<br>
    Alessandro e Jacopo. autori di una grande rimonta in questi ultimi 2 giorni, sono quasi alla pari.<br>
    Anche Ludovica, altra donna del gruppo, dovrà affrontare un esame importante: Alessandro R ha più di un centinaio di scommesse nel suo palmares. Saranno gestiti bene i 5.07 punti di vantaggio?<br>
    Mirko, vincitore un po' di tutto quest'anno, affronta, con un buon vantaggio, una persona poco nota. Pronostico scontato.<br>
    Chiude l'interessante sfida Andrea - Federico. Il cocchiere delle candeline dovrà vedersela con un altro membro della famiglia Ciufoli (accusato da tamponamento da una donna del nord) dopo il testa a testa al girone.<br>

    <h3>
        <strong>27 Giugno</strong>
    </h3>
    Oggi non mi dilungo troppo, mi tengo tutto per la grande serata di domani.<br>
    Celebriamo i tre eroi che si prendono di prepotenza il premio della critica e poi chi il primo posto globale, chi il passaggio del turno quasi assicurato e chi la mega rimonta.<br>
    L'incredibile 1 Sud Corea - Germania, quotato ben 15, viene pronosticato e preso da:<br>
    -<strong>Gozer</strong>, che vola e va in fuga nella classifica generale. Un giusto premio per chi al fantacalcio prende Kownacki già a Settembre e ha fatto concludere personaggi loschi come Mario Gallo a 0 titoli.<br>
    -<strong>Jacopo</strong>, in lotta continua tra la zona verde e quella dell'eliminazione, che ora si porta al secondo posto del gruppo B e domani può addirittura andare ad insidiare il primo.<br>
    -<strong>Fabio</strong>, il veterano dei mondiali che era in una posizione troppo anonima per le sue competenze. Il balzo dal penultimo al quarto posto nel girone B fa capire a chi ha abbandonato che tentare il miracolo non era una missione impossibile<br>
    Da un esperto, passiamo a menzionare due giovani: Salvatore che per 30 centesimi si prende la quarta posizione nel gruppo C (e la condivisione con Mirko del premio di maggior numero di partite indovinate) e Massimo che balza al secondo posto nel D.<br>
    <br>
    Domani ultime 4 partite. Ultime emozioni. Oggi si è dimostrato che nulla è ancora scritto.


    <h3>
        <strong>Regolamento ottavi</strong>
    </h3>
    -I partecipanti partiranno con i punti conquistati durante la fase a gironi.<br>
    -Il voto sarà privato (ovvero invisibile fino all'inizio del match). Questo può far storcere il naso a qualcuno ma è per evitare che chi sia in vantaggio vada a copiare l'avversario.<br>
    -In caso di parità, passerà chi aveva il miglior punteggio dopo la fase a gironi.<br>
    -In caso di ulteriore parità (ovvero se i due sfidanti erano partiti con lo stesso punteggio) conterà la miglior posizione in classifica al girone.<br>
    <strong>-Conterà il punteggio della partita nei 90 minuti (quindi non contando eventuali supplementari e rigori, altrimenti risultati come la X non avrebbero senso di esistere).</strong>
    <br>
    <h3>
        <strong>Regolamento quarti, semifinali e finali:</strong>
    </h3>
    -I punteggi saranno azzerati.<br>
    -Anche qui voto privato.<br>
    -In caso di parità, conterà il miglior punteggio ottenuto nel turno precedente.<br>
    <br>
    <strong>-Chi è stato eliminato potrà comunque continuare a giocare per la classifica generale.</strong><br>



    <p class="text-center m-t">
        <img src="{{ asset('img/panama.jpg') }}" alt="Panama" class="img-responsive" style="margin-left: auto; margin-right: auto"/>
    </p>
    <h3>
        <strong>Regole</strong>
    </h3>

    <p>Per ogni evento si scommette un singolo risultato e il punteggio totale sarà la somma delle quote indovinate.</p>

    <p>Un evento non indovinato o non pronosticato varrà 0 punti</p>

    <p>Per la fase a gironi le giocate possibili saranno 1, x, 2, 1x, 12, x2, Goal, No Goal, Over 2.5, Under 2.5. Non escludo dagli ottavi in poi di aggiungere anche marcatori e/o risultati esatti</p>

    <p>Quote prese da Planetwin. Gli eventi non presenti (esempio, l'1X di Belgio - Panama) vengono quotati 1.00</p>

    <p>Potete modificare ogni pronostico quante volte volete entro l'orario per cui è fissato il calcio d'inizio del match</p>

    <p>Ci saranno due campionati: quello che terrà conto dei punteggi totali e uno a gironi</p>

    <p>Nei gironi si qualificano le prime 4 di ogni raggruppamento. Agli ottavi gli incontri saranno del tipo<br>
        Ottavo 1: 1A-4B<br>
        Ottavo 2: 2A-3B<br>
        Ottavo 3: 3A-2B<br>
        Ottavo 4: 4A-1B<br>
        Ottavo 5: 1C-4D<br>
        Ottavo 6: 2C-3D<br>
        Ottavo 7: 3C-2D<br>
        Ottavo 8: 4C-1D<br>
    </p>

    <p>Durante il corso degli ottavi le squadre manterranno il punteggio del girone.</p>
    <p>Ai quarti di finale, semifinale e finale i punteggi vengono azzerati e le squadre partiranno da 0-0</p>

    <p>
        Il tabellone dei quarti sarà: <br>
        Quarto 1: Vincente Ottavo 1 - Vincente Ottavo 8<br>
        Quarto 2: Vincente Ottavo 2 - Vincente Ottavo 7<br>
        Quarto 3: Vincente Ottavo 3 - Vincente Ottavo 6<br>
        Quarto 4: Vincente Ottavo 4 - Vincente Ottavo 5<br>
    </p>

    <p>
        Il tabellone delle semifianli sarà: <br>
        Semifinale 1: Vincente Quarto 1 - Vincente Quarto 2<br>
        Semifinale 2: Vincente Quarto 3 - Vincente Quarto 4<br>
    </p>

    <p>
        Per quanto riguarda la finale sarà pronosticata sia la finale per il terzo e quarto posto che la finalissima.
    </p>

    <p>
        In caso di parità conterà il maggior numero di pronostici presi e in caso di ulteriore di parità, si vedrà prima la quota più alta indovinata e poi sorteggio.
    </p>
</div>

@stop
