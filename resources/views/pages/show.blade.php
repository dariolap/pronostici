<?php
use Illuminate\Support\Carbon ?>

@extends('page')

@section('css')
    @parent
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content_header')
    <h1>{{ config('app.name') }} - Elenco pronostici</h1>
@stop

@section('content')
@parent

@include('pages.partials.show.show_each', ['mappedEvents' => $today, 'text' => 'Match di oggi'])
@include('pages.partials.show.show_each', ['mappedEvents' => $future, 'text' => 'Match futuri'])

@if (!empty($past))
    @include('pages.partials.show.show_each', ['mappedEvents' => $past, 'text' => 'Match passati'])
@endif

@stop

