<!-- Message. Default to the left -->
<div class="direct-chat-msg {{ $comment->user()->first()->id === auth()->user()->id ? 'right' : '' }}">
    <div class="direct-chat-infos clearfix">
        <span class="direct-chat-name float-left">
            <strong>
                {{ $comment->user()->first()->name }}
            </strong>
        </span>
        <span class="direct-chat-timestamp float-right">{{ \Illuminate\Support\Carbon::parse($comment->created_at)->format('d/m/y H:i') }}
        @if ($comment->delete_authorized(\Illuminate\Support\Facades\Auth::user()->id))
                <i class="fa fa-trash delete-comment pointer" aria-hidden="true" c_id="{{ $comment->id }}"></i>
        @endif
        </span>
    </div>
    <!-- /.direct-chat-infos -->
    <img class="direct-chat-img" src="{{ $comment->user()->first()->getProfileImage() }}">
    <!-- /.direct-chat-img -->
    <div class="direct-chat-text">
        {{ $comment->comment }}
    </div>
<!-- /.direct-chat-text -->
</div>
<!-- /.direct-chat-msg -->
