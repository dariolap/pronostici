@extends('page')

@section('content_header')
    <h1>Bentornato {{ Auth::user()->name }}</h1>
@stop

@section('content')
    @parent

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-lg-3 col-6">

                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>{{ $totalBets }}</h3>
                        <p>{{ __('custom.total_bets')  }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-6">

                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ $winPercentageRecent }}<sup style="font-size: 20px">%</sup></h3>
                        <p>{{ __('custom.win_percentage')  }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-6">

                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{ $highestOverallOdd }}</h3>
                        <p>{{ __('custom.highest_overall_odd')  }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-6">

                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>{{ $highestOverallRecentOdd }}</h3>
                        <p>{{ __('custom.highest_overall_odd_recent')  }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                </div>
            </div>

        </div>


        <!-- Timelime example  -->
        <div class="row">
            <div class="col-md-12">
                <!-- The time line -->
                <div class="timeline">
                @foreach($newsfeeds as $n)
                    <!-- timeline time label -->
                        <div class="time-label">
                                <span class="bg-red">
                                     {{ \Illuminate\Support\Carbon::parse($n->created_at)->day . ' ' .
                                   __('months.'.\Illuminate\Support\Carbon::parse($n->created_at)->month) . ' ' .
                                   \Illuminate\Support\Carbon::parse($n->created_at)->year . ' ' }}
                                </span>
                        </div>
                        <div>

                            <div class="timeline-item">
                                    <span class="time"><i class="fas fa-clock"></i>
                                        {{ \Illuminate\Support\Carbon::parse($n->created_at)->format('H:i') }}</span>
                                </span>

                                @if(isset($n->title))
                                    <h3 class="timeline-header"><span class="text-success">
                                            <strong>{{ $n->title }}</strong>
                                    </h3>
                                @endif

                                <div class="timeline-body">
                                    {!! $n->news !!}
                                </div>

                            </div>
                            <div class="timeline-item mt-1">

                                @foreach($n->comments()->get() as $comment)
                                    <div class="card-body" style="padding: 0 1.25rem">
                                        <!-- Conversations are loaded here -->
                                        <div class="direct-chat-messages" style="padding-bottom: 0">
                                            @include('pages.partials.comment')
                                        </div>
                                    </div>
                            @endforeach

                            <!-- /.box-comment -->
                                <!-- /.box-footer -->
                                <div class="card-footer">
                                    <img class="img-responsive img-circle img-sm"
                                         src="{{ auth()->user()->getProfileImage() }}">
                                    <!-- .img-push is used to add margin to elements next to floating images -->
                                    <div class="img-push">
                                        <div class="input-group">
                                            <input type="text" class="form-control"
                                                   placeholder="{{ __('custom.Insert_comment') }}">
                                            <span class="input-group-btn">
                                                    <button class="btn btn-success send-comment"
                                                            newsfeed_id="{{ $n->id }}">Invia</button>
                                                </span>
                                        </div>
                                    </div>
                                </div>

                                <!-- /.box-footer -->
                            </div>

                        </div>

                        <!-- END timeline item -->
                        <!-- timeline item -->
                    @endforeach
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.timeline -->

    </section>
    <!-- /.content -->

@stop


@section('js')
    <script src="{{ asset('js/comments.js') }}"></script>
    <script>
        var config = {
            token: "{{ csrf_token() }}",
            routes: [
                {
                    comment_store: "{{ route('ajaxComments.store') }}",
                    comment_delete: "{{ route('ajaxComments.delete') }}",
                }
            ]
        };
    </script>
@endsection


