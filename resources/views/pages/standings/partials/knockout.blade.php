<div class="tab-pane fade {{ $knockoutPhases ? 'active show' : '' }} text-center" id="knockout" role="tabpanel"
     aria-labelledby="knockout-tab">
    <div class="text-22 bold">Eliminazione diretta</div>

    @foreach($knockoutPhases->reverse() as $phase)
        @if ($phase->groups)
            <div class="mt-5">
                <div class="text-18 bold">{{ $phase->name }}</div>
                <div class="row">
                    <div class="col-12">
                        @foreach($phase->groups as $group)
                            @foreach($group->users as $key => $player)
                                @php
                                    $phasePoints = $results->where('user_id', $player->id)->where('win', 1)->filter(function ($value) use ($phase) {
                                      return ($value->deadline >= $phase->start_date) && ($value->deadline <= $phase->end_date);
                                   })->sum('points') + $player->getOriginal('pivot_bonus');
                                $playerTemp[$key] = $player->name;
                                $score[$key] = number_format($phasePoints,2);
                                @endphp
                            @endforeach

                            @include('pages.standings.partials.bracket_item', [
                                  'player1' => $playerTemp[0],
                                  'score1' => $score[0],

                                  'player2' => $playerTemp[1],
                                  'score2' => $score[1],
                                  ])
                        @endforeach
                    </div>
                    @if ($phase->notes)
                        <div class="col-12 mt-3 text-center">
                            {{ $phase->notes }}
                        </div>
                    @endif
                </div>
            </div>
        @endif
    @endforeach

</div>
