<div class="tab-pane fade {{ $knockoutPhases->isEmpty() && !$groupsPhases->isEmpty() ? 'active show' : '' }}" id="groups" role="tabpanel" aria-labelledby="groups-tab">
    @foreach($groupsPhases as $phase)
        <div class="text-22 bold text-center mb-4">{{ $phase->name }}</div>

        @foreach($phase->groups as $group)
            <table class="table">
                <thead>
                <th colspan="4">
                    <div class="bold text-18 text-center">{{ $group->group }}</div>
                </th>
                </thead>
                <tbody>
                @foreach($group->users as $key => $player)
                    @php
                        $playersGroup[$key]['id'] = $player->id;
                        $playersGroup[$key]['name'] = $player->name;
                        $playersGroup[$key]['avatar'] = $player->getProfileImage();
                        $playersGroup[$key]['points'] = $results->where('user_id', $player->id)->where('win', 1)->filter(function ($value) use ($phase) {
                            return ($value->deadline >= $phase->start_date) && ($value->deadline <= $phase->end_date);
                        })->sum('points') + $player->getOriginal('pivot_bonus');
                        $playersGroup[$key]['total_bets'] = $results->where('user_id', $player->id)->where('win', '<>', 0)->filter(function ($value) use ($phase) {
                            return ($value->deadline >= $phase->start_date) && ($value->deadline <= $phase->end_date);
                        })->count();
                        $playersGroup[$key]['total_win'] = $results->where('user_id', $player->id)->where('win', 1)->filter(function ($value) use ($phase) {
                            return ($value->deadline >= $phase->start_date) && ($value->deadline <= $phase->end_date);
                        })->count();

                    @endphp
                @endforeach
                @php
                    $i = 1;
                  $players = Collect($playersGroup)->sortByDesc('points');
                @endphp
                @foreach($players as $player)
                    @if ($i<=$phase->n_qualified)
                        @php
                            $class = 'text-success';
                            $badgeClass = "bg-success";
                        @endphp
                    @elseif ($phase->n_playoff && $i>$phase->n_qualified && $i<=($phase->n_qualified + $phase->n_playoff))
                        @php
                            $class = 'text-warning';
                            $badgeClass = "bg-warning badge-white-text";
                        @endphp
                    @else
                        @php
                            $class = 'text-danger';
                            $badgeClass = "bg-danger";
                        @endphp
                    @endif
                    @if ($player['id'] == auth()->user()->id)
                        @php
                            $class .= ' bold';
                        @endphp
                    @endif
                    <tr>
                        <td width="10%" class="rank {{ $class }}">{{ $i++ }}</td>
                        <td width="60%" class="team {{ $class }}"><img class="img-circle img-sm avatar m-r" src="{{ $player['avatar'] }}">{{$player['name'] }}</td>
                        <td width="20%" class="{{ $class }}">{{ $player['total_win'] }} / {{ $player['total_bets'] }}</td>
                        <td width="10%" class="points"><span class="badge {{ $badgeClass }}">{{ number_format($player['points'], 2) }}</span></td>
                    </tr>

                @endforeach

                @php
                    unset($playersGroup)
                @endphp

                </tbody>
            </table>
        @endforeach
        @if ($phase->notes)
            <p>
                {{ $phase->notes }}
            </p>
        @endif
    @endforeach
</div>
