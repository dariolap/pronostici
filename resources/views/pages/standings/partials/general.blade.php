<div class="tab-pane fade {{ $knockoutPhases->isEmpty() && $groupsPhases->isEmpty() ? 'active show' : '' }}"
     id="general" role="tabpanel" aria-labelledby="general-tab">
    @if (!$points->isEmpty())
        <div class="text-22 bold text-center mb-4">Classifica Generale</div>
        <div class="standing_wrapper">
            <table class="datatable responsive stripe">
                <thead>
                    <th data-priority="1" class="text-left">
                        Pos
                    </th>
                    <th data-priority="1">
                        Nome
                    </th>
                    <th data-priority="3">
                        Indovinate / Totali Giocate
                    </th>
                    <th data-priority="1">
                        Punti
                    </th>
                    <th data-priority="2">
                        Visualizza
                    </th>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($points as $index => $point)
                    @if($i == 1)
                        @php
                            $class = 'text-success';
                            $badgeClass = 'bg-success';
                        @endphp

                    @elseif($i == 2 || $i == 3)
                        @php
                            $class = 'text-info';
                            $badgeClass = 'bg-info';
                        @endphp

                    @else
                        @php
                            $class = '';
                            $badgeClass = 'bg-primary';
                        @endphp
                    @endif
                    @if ($point->user_id == auth()->user()->id)
                        @php
                            $class .= ' bold';
                        @endphp
                    @endif
                    <tr>
                        <td class="rank {{ $class }} text-left" style="width: 20%">{{ $i++ }}</td>
                        <td class="team {{ $class }}" style="width: 55%">
                            <span class="{{ $class }}">
                                <img class="img-circle img-sm avatar d-none d-sm-block mr" src="{{ $point->user->getProfileImage() }}">{{ $point->user->name }}
                            </span>
                        </td>
                        <td class="points {{ $class }}" style="width: 10%">
                            {{ $point->total_wins }}/{{ $point->total_results }}
                        </td>
                        <td class="points" style="width: 10%">
                            <span class="badge {{ $badgeClass }}">{{ number_format($point->points, 2) }}</span>
                        </td>
                        <td style="width: 5%" class="text-center">
                            <a href="{{ route('pages.results', ['user_id' => $point->user->id, 'tournament_id' => $tournament->id]) }}">
                                <button class="btn btn-primary btn-xs"><i class="fas fa-eye"></i></button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        @if ($maxBet->isNotEmpty() || $bestWinCount->isNotEmpty() || !empty($bestWoman))
            <div class="row m-t">
                <div class="col-xs-12">
                    <div class="statistics text-18">
                        <p class="text-primary"><strong>{{ __('custom.maxBet') }}</strong>:<br>
                        @foreach ($maxBet as $bet)
                            <p><img class="img-circle img-sm avatar m-r"
                                    src="{{ config('custom.users_images_path') . $bet->avatar }}">{{ $bet->name }}
                                : {{ $bet->points }}
                                con {{ $bet->event }} {{ $bet->bet_type }}
                            </p>
                            @endforeach
                            </p>
                            <br>
                            <p class="text-primary"><strong>{{ __('custom.bestWinCount') }}</strong>:<br>
                            @foreach ($bestWinCount as $win)
                                <p>
                                    <img class="img-circle img-sm avatar m-r"
                                         src="{{ config('custom.users_images_path') . $win->avatar }}">{{ $win->name }}
                                    : {{ $win->c }}
                                </p>
                                @endforeach
                                </p>
                                <br>
                                {{--@if ($bestWoman)
                                            <p class="text-primary"><strong>{{ __('custom.bestWoman') }}</strong>:<br>
                                            <p><img class="img-circle img-sm avatar m-r" src="{{ config('custom.users_images_path') . $bestWoman->user->avatar }}">{{ $bestWoman->user->name }}: {{ $bestWoman->points }}</p>
                                    </p>
                                @endif--}}
                    </div>
                </div>
            </div>

        @endif
    @else
        {{--Ancora nessun punteggio calcolato--}}
        <div class="text-22 bold text-center mb-4">Elenco Iscritti</div>
        <div class="standing_wrapper">
            <div class="row mt-3">
                @foreach ($users as $user)
                    <div class="col-md-3 col-6 mt-1">
                            <span>
                                <img class="img-circle img-sm avatar m-r d-none d-sm-block"
                                     src="{{ $user->getProfileImage() }}">{{ $user->name }}
                            </span>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
