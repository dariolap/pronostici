{{-- resources/views/admin/dashboard.blade.php --}}

@extends('page')

@section('title', __('menu.old_tournaments'))

@section('content_header')
    <h1>{{  __('menu.old_tournaments') }}</h1>
@stop

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="card">
        <table class="datatable responsive table-layout-fixed stripe">
            <thead>
            <th>
                Nome
            </th>
            <th class="text-center">
                Visualizza
            </th>

            </thead>
            <tbody>
            @foreach($tournaments as $tournament)
                <tr>
                    <td>
                        {{$tournament->name}}
                    </td>
                    <td class="text-center">
                        <a href="{{ route('pages.standings', $tournament->id) }}" class="btn btn-primary btn-xs">
                            <i class="fas fa-eye"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@include('partials.datatable')
