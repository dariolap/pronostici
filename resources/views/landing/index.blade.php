<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="Grayrids">
    <title>The Betting Game</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/essence/bootstrap.min.css">
    <link rel="stylesheet" href="css/essence/line-icons.css">
    <link rel="stylesheet" href="css/essence/owl.carousel.css">
    <link rel="stylesheet" href="css/essence/owl.theme.css">
    <link rel="stylesheet" href="css/essence/nivo-lightbox.css">
    <link rel="stylesheet" href="css/essence/magnific-popup.css">
    <link rel="stylesheet" href="css/essence/animate.css">
    <link rel="stylesheet" href="css/essence/menu_sideslide.css">
    <link rel="stylesheet" href="css/essence/main.css?v0.0.2">
    <link rel="stylesheet" href="css/essence/responsive.css">
    <link rel="stylesheet" href="css/plugin/sweetalert2.min.css">

</head>

<body>
<!-- Header Section Start -->
<div id="app">
    <header id="slider-area">
        <nav class="navbar navbar-expand-md fixed-top scrolling-navbar bg-white">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="img/title.png"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="lni-menu"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item"><a class="nav-link" data-toggle="modal" data-target="#modal-login"
                                                href="#modal-login">{{ __('adminlte.sign_in') }}</a>
                        </li>
                        <li class="nav-item-right"></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="modal" data-target="#modal-signup"
                                                href="#modal-signup">{{ __('adminlte.register_a_new_membership') }}</a>
                        </li>
                        <li class="nav-item-right"></li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#modal-regulation"
                               href="#modal-regulation">Regolamento</a>
                        </li>

                    </ul>
                    <!--                    <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#slider-area">Home</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#services">Services</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#features">Features</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#portfolios">Works</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#pricing">Pricing</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#team">Team</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#subscribe">Subscribe</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#blog">Blog</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link page-scroll" href="#contact">Contact</a>
                                            </li>
                                        </ul>-->
                </div>
            </div>
        </nav>

        <!-- Main Carousel Section -->
        <div id="carousel-area">
            <div id="carousel-slider" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="carousel-caption text-left">
                            <!--                        <h3 class="wow fadeInRight" data-wow-delay="0.2s">Handcrafted</h1>-->
                            <h2 class="wow fadeInRight page-title" data-wow-delay="0.4s">The Betting Game</h2>
                            <h4 class="wow fadeInRight" data-wow-delay="0.6s">Il gioco a pronostici <b>GRATUITO</b> con
                                le quote dei bookmaker</h4>

                            <button href="#modal-login"
                                    class="modal-trigger btn btn-lg btn-common btn-effect wow fadeInRight"
                                    data-toggle="modal" data-target="#modal-login"
                                    data-wow-delay="0.9s">{{ __('adminlte.sign_in') }}
                            </button>
                            <button href="#modal-signup"
                                    class="modal-trigger btn btn-lg btn-dark btn-effect wow fadeInRight"
                                    data-toggle="modal" data-target="#modal-signup"
                                    class="btn btn-lg btn-border wow fadeInRight" data-wow-delay="1.2s">
                                {{ __('adminlte.register_a_new_membership') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <!-- Header Section End -->

    <!-- Services Section Start -->
    <section id="services" class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.2s">
                        <div class="icon color-1">
                            <i class="lni-stats-up"></i>
                        </div>
                        <h4>Gioco con le quote</h4>
                        <p>Gioca contro altri utenti, in modo totalmente gratuito, puntando la tua scommessa. Più
                            difficile l'esito pronosticato più punti guadagni.<br>

                            Si punta su uno degli esiti disponibili per ogni evento e, in caso di risultato indovinato,
                            si riceve un punteggio pari al valore della quota puntata.</p>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.4s">
                        <div class="icon color-2">
                            <i class="lni-timer"></i>
                        </div>
                        <h4>Semplice modalità di punteggio</h4>
                        <p>Il punteggio totale del partecipante sarà la somma delle quote indovinate.

                            Un evento non indovinato o non pronosticato varrà 0 punti.

                            Si può modificare ogni pronostico quante si vuole entro l'orario per cui è fissato il calcio
                            d'inizio del match.</p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="item-boxes services-item wow fadeInDown" data-wow-delay="0.6s">
                        <div class="icon color-3">
                            <i class="lni-cup"></i>
                        </div>
                        <h4>Preparati per l'Europa League 2025!</h4>
                        <!--                        <p>Due modalità di torneo, una che tiene conto solo del punteggio totale (a cui potranno
                                                    partecipare tutti dal primo match fino alla finale) e l'altra con gironi + eliminazione
                                                    diretta!<br>
                                                    <br>
                                                    Nella prima fase i partecipanti saranno suddivisi in gironi, di cui solo i migliori
                                                    punteggi del raggruppamento, al termine della prima fase, accederanno agli ottavi.<br>
                                                    <br>-->
                        Per saperne di più leggi le nostre <a data-toggle="modal" data-target="#modal-regulation"
                                                              href="#modal-regulation">regole</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Section End -->

    <!-- Call to Action Start -->
    <section class="call-action section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="cta-trial text-center">
                        <h3>Sei pronto per iniziare?</h3>
                        <p>Che aspetti ad unirti? Le iscrizioni chiudono il 13 Febbraio alle ore 18.45</p>
                        <div class="row">
                            <div class="col-12">

                                <a class="nav-link btn btn-primary btn-effect btn-lg" data-toggle="modal"
                                   data-target="#modal-signup"
                                   href="#modal-signup">Unisciti Gratis!</a>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <a class="nav-link btn btn-danger btn-effect btn-lg" data-toggle="modal"
                                   data-target="#modal-regulation"
                                   href="#modal-regulation">Leggi il regolamento</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Call to Action End -->

    <!-- Portfolio Section -->
    <section id="portfolios" class="section">
        <!-- Container Starts -->
        <div class="container">
            <div class="section-header">
                <h2 class="section-title">I nostri partner</h2>
                <!--                <p class="section-subtitle">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos
                                    debitis.</p>-->
            </div>
            <!-- Portfolio Controller/Buttons -->

            <div class="row">
                <div class="col-4 text-center">
                    <a href="https://t.me/piccoli_comuni_italiani">
                        <img class="circle responsive-img rounded-circle" style="width: 100px; height: 100px"
                             src="{{ asset('img/piccolicomuniitaliani.jpg') }}">
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="https://t.me/BarSportNews">
                        <img class="circle responsive-img rounded-circle" style="width: 100px; height: 100px"
                             src="{{ asset('img/barsport_logo.png') }}">
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="https://www.instagram.com/dasportit/">
                        <img class="circle responsive-img rounded-circle" style="width: 100px; height: 100px"
                             src="{{ asset('img/dasport.jpg') }}">
                    </a>
                </div>

            </div>

        </div>
    </section>
</div>
<!-- Portfolio Section Ends -->

<!-- Counter Section Start -->
<div class="counters section bg-defult">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="facts-item">
                    <div class="icon">
                        <i class="lni-rocket"></i>
                    </div>
                    <div class="fact-count">
                        <h3><span class="counter">{{ $users }}</span></h3>
                        <h4>Utenti iscritti</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="facts-item">
                    <div class="icon">
                        <i class="lni-coffee-cup"></i>
                    </div>
                    <div class="fact-count">
                        <h3><span class="counter">{{ $events }}</span></h3>
                        <h4>Partite giocate</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="facts-item">
                    <div class="icon">
                        <i class="lni-user"></i>
                    </div>
                    <div class="fact-count">
                        <h3><span class="counter">{{ $results }}</span></h3>
                        <h4>Pronostici effettuati</h4>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="facts-item">
                    <div class="icon">
                        <i class="lni-heart"></i>
                    </div>
                    <div class="fact-count">
                        <h3><span class="counter">{{ $tournaments }}</span></h3>
                        <h4>Tornei realizzati</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Counter Section End -->

<!-- Modals -->
@if ($canSendMessage)
    @include('landing.partials.contact_form')
@endif
@include('landing.partials.modal_login')
@include('landing.partials.modal_signup')
@include('landing.partials.modal_regulation')

<!-- Footer Section Start -->
<footer>

    <!-- Copyright Start  -->
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="site-info float-left">
                        <p>Made by <a class="text-lighten-3"
                                      href="https://www.linkedin.com/in/dario-la-padula-ab770780/">Dario La
                                Padula</a></p>
                    </div>
                    <div class="float-right">
                        <ul class="nav nav-inline">
                            <!--                            <li class="nav-item">
                                                            <a class="nav-link active" href="#">About Prime</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">TOS</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" href="#">Return Policy</a>
                                                        </li>-->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="modal" data-target="#modal-regulation"
                                   href="#modal-regulation">Regolamento</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright End -->

</footer>
<!-- Footer Section End -->

<!-- Go To Top Link -->
<a href="#" class="back-to-top">
    <i class="lni-arrow-up"></i>
</a>

<div id="loader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="js/essence/jquery-min.js"></script>
<script src="js/essence/popper.min.js"></script>
<script src="js/essence/bootstrap.min.js"></script>
<script src="js/essence/classie.js"></script>
<script src="js/essence/jquery.mixitup.js"></script>
<script src="js/essence/nivo-lightbox.js"></script>
<script src="js/essence/owl.carousel.js"></script>
<script src="js/essence/jquery.stellar.min.js"></script>
<script src="js/essence/jquery.nav.js"></script>
<script src="js/essence/scrolling-nav.js"></script>
<script src="js/essence/jquery.easing.min.js"></script>
<script src="js/essence/wow.js"></script>
<script src="js/essence/jquery.vide.js"></script>
<script src="js/essence/jquery.counterup.min.js"></script>
<script src="js/essence/jquery.magnific-popup.min.js"></script>
<script src="js/essence/waypoints.min.js"></script>
<script src="js/essence/form-validator.min.js"></script>
<script src="js/essence/contact-form-script.js"></script>
<script src="js/essence/main.js"></script>
<script src="js/plugin/sweetalert2.all.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<script>
    $(document).ready(function () {
        //$('.modal').modal();
        @if(session()->get('error') == 'login')
        $(function () {
            $('#modal-login').modal();
        });
        @endif
        @if(session()->get('error') == 'signup')
        $(function () {
            $('#modal-signup').modal();
        });
        @endif
    });

    document.addEventListener('DOMContentLoaded', function () {
        const submitButton = document.getElementById('submitBtn');
        const form = document.getElementById('contactForm');

        // Funzione per verificare se il form è valido
        function validateForm() {
            const inputs = form.querySelectorAll('input, textarea');
            let isValid = true;

            inputs.forEach(input => {
                if (!input.checkValidity()) {
                    isValid = false;
                }
            });

            // Abilita o disabilita il pulsante
            submitButton.disabled = !isValid;
        }

        // Event listener per input e modifica del form
        form.addEventListener('input', validateForm);

        // Inizializza lo stato del pulsante
        validateForm();

        submitButton.addEventListener('click', function (e) {
            e.preventDefault(); // Evita il comportamento predefinito del bottone

            const formData = new FormData(form); // Raccogli i dati del form
            submitButton.disabled = true; // Disabilita il bottone per prevenire invii multipli

            fetch(form.action, {
                method: 'POST',
                body: formData,
                headers: {
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                }
            })
                .then(response => {
                    if (response.ok && response.status && response.status === 200) {
                        Swal.fire({
                            icon: 'success',
                            text: "{!!  __('landing.contact.success') !!}"
                        });
                        $('#contact').fadeOut('slow', function () {
                            $(this).remove();
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            text: "{!!  __('landing.contact.error') !!}"
                        });
                        submitButton.disabled = false;
                    }
                });
        });
    });
</script>


</body>
</html>
