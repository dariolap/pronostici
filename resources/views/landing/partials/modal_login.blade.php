<!-- Modal Body -->
<div id="modal-login" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        {{ trans('adminlte.login_message') }}
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! csrf_field() !!}

                    @if(Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>

                    @endif

                    @if(Session::has('message'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="email-login">Email</label>
                        <input class="form-control" id="email-login" type="email" name="email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="password-login">Password</label>
                        <input class="form-control" id="password-login" type="password" name="password" required>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="remember" name="remember" checked="true">
                            <label class="form-check-label" for="remember">{{ __('adminlte.remember_me') }}</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">


                    <button type="button" class="btn btn-secondary btn-circle btn-large"
                            data-dismiss="modal" aria-label="Close">
                        {{ __('custom.close') }}
                    </button>
                    <button type="submit"
                            class="btn btn-primary btn-circle btn-large">{{ __('custom.login') }}
                    </button>
                </div>
                <div class="modal-footer" style="border-top: none">
                    <a
                            href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}">{{ __('adminlte.i_forgot_my_password') }}</a>
                </div>

            </div>
        </form>
    </div>
</div>

