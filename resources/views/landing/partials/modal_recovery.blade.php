<!-- Modal Body -->
<div id="modal-recovery" class="modal">
    <div class="modal-content">
        <div class="row">
            <a href="#!" class="right modal-close red waves-effect btn-floating">
                <i class="material-icons">close</i>
            </a>
        </div>

        <div class="row">

            <div class="center-align">
                <img src="{{ asset('img/favicon.png') }}" style="width:50px; height:50px" rel="shortcut icon">
            </div>
            <p class="center">{{ trans('adminlte::adminlte.password_reset_message') }}</p>
            <form action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
                {!! csrf_field() !!}

                @if(Session::has('success'))
                    <div class="alert card green lighten-4 green-text text-darken-4">
                        <div class="card-content">
                            <p><i class="material-icons">done</i><span>{{ Session::get('success') }}</span></p>
                        </div>
                    </div>

                @endif

                @if(Session::has('message'))
                    <div class="alert card red lighten-4 red-text text-darken-4">
                        <div class="card-content">
                            <p><i class="material-icons">report</i><span>{{ Session::get('message') }}</span></p>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">email_outline</i>
                        <input type="email" name="email" id="email-recovery" class="form-control"
                               value="{{ old('email') }}" required>
                        <label for="email-recovery" data-error="wrong"
                               data-success="right">{{ trans('adminlte::adminlte.email') }}</label>
                        @if ($errors->has('email'))
                            <span class="red-text">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <button type="submit"
                                class="btn btn-large btn-circle waves-effect waves-light col s12">{{ trans('adminlte::adminlte.send_password_reset_link') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
