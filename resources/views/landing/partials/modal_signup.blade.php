<div id="modal-signup" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{ url(config('adminlte.register_url', 'signup')) }}" method="post">
            <input type="hidden" name="full_name" value="">
            <input type="hidden" name="form_start_time" value="{{ now()->timestamp }}">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="modal-title">
                        {{ trans('adminlte.register_message') }}
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! csrf_field() !!}

                    @if(Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>

                    @endif

                    @if(Session::has('message'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="name-signup" data-error="wrong"
                               data-success="right">{{ trans('adminlte::adminlte.full_name') }}</label>
                        <input type="text" name="name" id="name-signup" class="form-control" value="{{ old('name') }}"
                               required>
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                <div class="form-group">
                    <label for="email-signup" data-error="wrong"
                           data-success="right">{{ trans('adminlte::adminlte.email') }}</label>
                    <input type="email" name="email" id="email-signup" class="form-control"
                           value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-signup">{{ trans('adminlte::adminlte.password') }}</label>
                    <input type="password" name="password" id="password-signup" class="form-control"
                           value="{{ old('password') }}" required>
                    @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-repeat-signup" data-error="wrong"
                           data-success="right">{{ trans('adminlte::adminlte.retype_password') }}</label>
                    <input type="password" name="password_repeat" id="password-repeat-signup" class="form-control"
                           value="{{ old('password_repeat') }}" required>
                    @if ($errors->has('password_repeat'))
                        <span class="text-danger">{{ $errors->first('password_repeat') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="{{ config('captcha.recaptcha_site_key') }}"></div>
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-circle btn-large"
                        data-dismiss="modal" aria-label="Close">
                    {{ __('custom.close') }}
                </button>
                <button type="submit"
                        class="btn btn-primary btn-circle btn-large">{{ trans('adminlte::adminlte.register') }}
                </button>
            </div>
            </div>
        </form>
    </div>
</div>

