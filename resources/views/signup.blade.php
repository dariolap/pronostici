@extends('master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css?v0.0.4') }}">
    @yield('css')
@stop

@section('body_class', 'register-page')

@section('body')
    <div class="register-box">
        
        <div class="register-box-body">
            <div class="login-logo">
                <img src="{{ asset('img/favicon.png') }}" style="width:50px; height:50px" rel="shortcut icon">
            </div>
            <p class="login-box-msg">{{ trans('adminlte.register_message') }}</p>
            <form action="{{ url(config('adminlte.register_url', 'signup')) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                    <input type="hidden" name="full_name" value="">
                    <input type="hidden" name="form_start_time" value="{{ now()->timestamp }}">
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                           placeholder="{{ trans('adminlte::adminlte.full_name') }}" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.password') }}" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_repeat') ? 'has-error' : '' }}">
                    <input type="password" name="password_repeat" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.retype_password') }}" required>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_repeat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_repeat') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit"
                        class="btn btn-primary btn-block btn-flat"
                >{{ trans('adminlte::adminlte.register') }}</button>
            </form>
            <div class="text-center auth-links">
                <a href="{{ url(config('adminlte.login_url', 'login')) }}"
                   class="bold">{{ trans('adminlte.i_already_have_a_membership') }}</a>
            </div>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    @yield('js')
    <script>
        $(".btn-refresh").click(function(){
            $.ajax({
                type:'GET',
                url: '{{ route('refresh_captcha') }}',
                success:function(data){
                    $(".captcha span").html(data.captcha);
                }
            });
        });
    </script>
@stop
