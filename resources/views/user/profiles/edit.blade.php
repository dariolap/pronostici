@extends('page')

@section('content_header')
    <h1>{{ config('app.name') }} - {{ __('custom.edit_profile') }}</h1>
@stop

@section('content')
    @parent

    <div class="card">
        <div class="card-body">
            <!-- general form elements -->
                <!-- /.box-header -->
                <form id="edit_profile" method="POST" action="{{ route('auth.user.update') }}" autocomplete="off" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}

                        <div class="form-group">
                            <strong>{{ __('custom.full_name') }}</strong>
                            <input type="text" name="name" class="form-control"
                                   placeholder="{{ __('custom.full_name') }}" value="{{ old('name', $user->name) }}"
                                   required>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <strong>{{ __('custom.email') }}</strong>
                            <input type="email" name="email" class="form-control"
                                   placeholder="{{ __('custom.email') }}" value="{{ old('email', $user->email) }}"
                                   readonly disabled>
                        </div>

                        <div class="form-group">
                            <label>{{ __('custom.edit_password') }}</label>
                            <input id="password" type="password" name="password" class="form-control edit_password"
                                   placeholder="{{ __('custom.edit_password') }}">
                            <span class="text-danger">
                            @if ($errors->has('password'))
                                    {{ $errors->first('password') }}
                                @endif
                            </span>
                        </div>

                        <div class="form-group">
                            <label>{{ __('custom.password_confirmation') }}</label>
                            <input id="password_confirm" type="password" name="password_confirmation"
                                   class="form-control"
                                   placeholder="{{ __('custom.password_confirmation') }}">
                            <span class="text-danger">
                            @if ($errors->has('password_confirmation'))
                                    {{ $errors->first('password_confirmation') }}
                                @endif
                            </span>
                        </div>

                        <div class="row">
                            <div class="col-md-2 col-sm-12 col-xs-12 text-center">
                                <img id="avatar" style="width: 100px; height: 100px; margin-bottom:10px" class="img-circle" src="{{ $user->getProfileImage() }}">
                                <div class="m-t">
                                    <button type="button" class="btn btn-warning text-white btn-remove">{{ __('custom.remove') }}</button>
                                </div>
                            </div>
                            <div class="col-md-10 col-sm-12 col-xs-12 m-t">
                                <div class="form-group">
                                    <label>{{ __('custom.image_profile_change') }}</label>
                                    <input id="file" type="file" class="form-control" value="{{ $user->avatar }}" name="avatar" accept="image/*">
                                    <span class="text-danger file-error">
                                    @if ($errors->has('avatar'))
                                        {{ $errors->first('avatar') }}
                                    @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary btn-submit">{{ __('custom.save') }}</button>
                        </div>
                </form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/edit_profile.js') }}"></script>
    <script>
        var config = {
            token: "{{ csrf_token() }}",
            user_id: "{{ auth()->user()->id }}",
            default_image: " {{config('custom.users_images_path') . config('custom.default_user_image') }}",
            routes: [
                {
                    remove_avatar: "{{ route('ajaxUsers.remove_avatar') }}",
                }
            ]
        };
    </script>
@endsection

