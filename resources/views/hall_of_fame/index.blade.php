@extends('page')

@section('content_header')
    <h1>{{ __('custom.champions_list') }}</h1>
@stop

@section('content')
    @parent

    <div class="card">
            <table class="datatable responsive stripe">
                <thead class="text-primary">
                <tr>
                    <th>
                        <strong>Torneo</strong>
                    </th>
                    <th>
                        <strong>Vincitore</strong>
                    </th>
                    <th>
                        Miglior Punteggio totale
                    </th>
                    <th>
                        Maggior quota indovinata
                    </th>
                    <th>
                        Maggior numero di eventi indovinati
                    </th>
                </tr>
                </thead>
                <tbody>

                @foreach ($hallOfFames as $hallOfFame)

                    <tr>
                        <th class="text-success" scope="row"><strong>{{ $hallOfFame->tournament->name }}</strong></th>
                        <td>@if($hallOfFame->first_user_ids)
                            <strong>
                                @foreach ($hallOfFame->firstUsers() as $user)
                                    {{ $user->name . (!$loop->last ? ', ' : '') }}
                                @endforeach
                            </strong>
                            @else
                                Solo classifica generale
                            @endif
                        </td>
                        <td>
                            @foreach ($hallOfFame->firstPointsUsers() as $user)
                                @if(!$hallOfFame->first_user_ids)<strong>@endif
                                    {{ $user->name . (!$loop->last ? ', ' : '') }}
                                @if(!$hallOfFame->first_user_ids)</strong>@endif
                            @endforeach {{ '(' . number_format($hallOfFame->total_points_first, 2) . ')' }}
                        </td>
                        <td>
                            @foreach ($hallOfFame->best_odd_wins as $value)
                                @foreach ($value['user_ids'] as $user_id)
                                    {{ $bestOddWinUsers[$user_id] . (!$loop->last ? ', ' : '') }}
                                @endforeach
                                {{ __('custom.with') }} {{ $value['description'] }}<br>
                            @endforeach {{ '(' . $hallOfFame->total_high_prediction_win . ')' }}
                        </td>

                        <td>
                            @foreach ($hallOfFame->highPredictionWinUsers() as $user)
                                {{ $user->name . (!$loop->last ? ', ' : '') }}
                            @endforeach {{ '(' . $hallOfFame->total_high_prediction_win . ')' }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    </div>
@stop

@include('partials.datatable')

