<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Controllers\LandingController;
use Tests\TestCase;

/**
 * Class LandingControllerTest.
 *
 * @covers \App\Http\Controllers\LandingController
 */
class LandingControllerTest extends TestCase
{
    /**
     * @var LandingController
     */
    protected $landingController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->landingController = new LandingController();
        $this->app->instance(LandingController::class, $this->landingController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->landingController);
    }

    public function testIndex(): void
    {
        /** @todo This test is incomplete. */
        $this->get('/home')
            ->assertStatus(200);
    }

    public function testRules(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }
}
