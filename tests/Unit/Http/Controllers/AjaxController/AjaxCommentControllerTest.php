<?php

namespace Tests\Feature\Http\Controllers\AjaxController;

use App\Http\Controllers\AjaxController\AjaxCommentController;
use Tests\TestCase;

/**
 * Class AjaxCommentControllerTest.
 *
 * @covers \App\Http\Controllers\AjaxController\AjaxCommentController
 */
class AjaxCommentControllerTest extends TestCase
{
    /**
     * @var AjaxCommentController
     */
    protected $ajaxCommentController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->ajaxCommentController = new AjaxCommentController();
        $this->app->instance(AjaxCommentController::class, $this->ajaxCommentController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->ajaxCommentController);
    }

    public function testStore(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }

    public function testDelete(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
