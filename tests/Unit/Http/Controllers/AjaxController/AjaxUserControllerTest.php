<?php

namespace Tests\Feature\Http\Controllers\AjaxController;

use App\Http\Controllers\AjaxController\AjaxUserController;
use Tests\TestCase;

/**
 * Class AjaxUserControllerTest.
 *
 * @covers \App\Http\Controllers\AjaxController\AjaxUserController
 */
class AjaxUserControllerTest extends TestCase
{
    /**
     * @var AjaxUserController
     */
    protected $ajaxUserController;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->ajaxUserController = new AjaxUserController();
        $this->app->instance(AjaxUserController::class, $this->ajaxUserController);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->ajaxUserController);
    }

    public function testRemoveAvatar(): void
    {
        /** @todo This test is incomplete. */
         $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
