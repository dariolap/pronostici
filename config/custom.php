<?php

return [
    'default_user_image'            => 'default.png',
    'users_images_path'             =>  env('APP_URL') . '/storage/uploads/avatars/',
    'default_tournament_logo'       => 'default.png',
    'tournaments_logo_path'         =>  env('APP_URL') . '/storage/uploads/tournaments/',

];

?>
