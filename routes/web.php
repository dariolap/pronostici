<?php

use App\Http\Controllers\Admin\HallOfFameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* MAIN */
Auth::routes(['register' => false, 'login' => false]);

Route::get('/home', 'LandingController@index');

Route::get('/login', 'LandingController@index')->name('login.get');
Route::get('refresh_captcha', 'UserController@refreshCaptcha')->name('refresh_captcha');
Route::post('/login', 'UserController@checkLogin')->name('login.post');
Route::post('/logout', 'UserController@logout')->name('logout');
Route::get('/signup', 'UserController@signup')->name('signup.get');
Route::post('/signup', 'UserController@store')->middleware('throttle:5,1')->name('signup.post'); // 5 tentativi al minuto
Route::get('/', 'LandingController@index')->name('landing.index');
Route::get('/regolamento', 'LandingController@rules')->name('landing.rules');
Route::post('/send_contact_mail', 'LandingController@send_contact_email')->name('landing.send_contact_email');
/* AUTH */
Route::group(['middleware' => 'auth'], function () {

    Route::prefix('auth')->group(function () {
        Route::get('/index', 'PagesController@index')->name('pages.index');
        Route::get('/general_regulation', 'PagesController@general_regulation')->name('pages.general_regulation');
        Route::get('/tournaments_available', 'PagesController@tournaments_available')->name('pages.tournaments_available');
        Route::get('/old_tournaments', 'PagesController@old_tournaments')->name('pages.old_tournaments');
        Route::post('/add_tournament_user', 'AjaxController\AjaxPagesController@store_tournament_user')->name('pages.add_tournament_user');
        Route::get('/dashboard/{tournament_id}', 'PagesController@dashboard')->name('pages.dashboard');
        Route::get('/tournament_regulation/{tournament_id}', 'PagesController@tournament_regulation')->name('pages.tournament_regulation');
        Route::get('/add/{id}', 'PagesController@add')->name('pages.add');
        Route::post('/add/{tournament_id}', 'PagesController@store')->name('pages.store');
        Route::get('/show/{tournament_id}', 'PagesController@show')->name('pages.show');
        Route::get('/event/{id}/{tournament_id?}', 'PagesController@event')->name('pages.event');
        Route::post('/event_ajax', 'PagesController@eventAjax')->name('pages.eventAjax');
        Route::get('/results/{user_id?}/{tournament_id?}', 'PagesController@results')->name('pages.results');
        Route::get('/standings/{tournament_id}', 'PagesController@standings')->name('pages.standings');
        Route::get('/messages/{tournament_id}', 'PagesController@messages')->name('pages.messages');
        Route::post('/messages/{tournament_id}', 'PagesController@store_message')->name('pages.store_message');
        Route::get('/champions_list', 'PagesController@champions_list')->name('pages.champions_list');
        Route::get('/hall_of_fame', 'HallOfFameController@index')->name('hall_of_fame.index');
        Route::post('/survey', 'SurveyController@survey')->name('survey');
        Route::get('/contact_us', 'PagesController@contact_us')->name('pages.contact_us');
        Route::post('/send_contact_mail', 'PagesController@send_contact_mail')->name('pages.send_contact_mail');

        Route::group(['middleware' => 'auth.admin'], function () {

            /* AUTH ADMIN */
            Route::prefix('admin')->group(function () {
                /* EVENT ADMIN */
                Route::get('/events/get_available', 'Admin\EventController@available_online')->name('admin.event.available_online');
                Route::get('/events/get_available_multisport', 'Admin\EventController@available_online_multisport')->name('admin.event.available_online_multisport');
                Route::get('/events/add/{id?}', 'Admin\EventController@add_reloadbet_event')->name('admin.event.add');
                Route::get('/events/add_by_file/{filename}', 'Admin\EventController@add_by_file')->name('admin.event.add_by_file');
                Route::get('/events/add_multisport/{sport?}/{id?}', 'Admin\EventController@add_multisport')->name('admin.event.add_multisport');
                Route::post('/events/add/{id?}', 'Admin\EventController@store')->name('admin.event.store');
                Route::get('/events/import', 'Admin\EventController@import')->name('admin.event.import');

                Route::get('/categories/add', 'AdminController@add_category')->name('admin.categories.add');
                Route::post('/categories/add', 'AdminController@store_category')->name('admin.categories.store');

                /* NEWSFEED ADMIN */
                Route::get('/newsfeeds', 'Admin\NewsfeedController@index')->name('admin.newsfeeds.index');
                Route::get('/newsfeeds/add', 'Admin\NewsfeedController@create')->name('admin.newsfeeds.add');
                Route::post('/newsfeeds/store', 'Admin\NewsfeedController@store')->name('admin.newsfeeds.store');
                Route::post('/newsfeeds/remove/{id?}', 'Admin\NewsfeedController@delete')->name('admin.newsfeeds.remove');

                /* TOURNAMENTS */
                Route::get('/tournaments/index', 'Admin\TournamentController@index')->name('admin.tournaments.index');
                Route::get('/tournaments/add', 'Admin\TournamentController@add')->name('admin.tournaments.add');
                Route::post('/tournaments/add', 'Admin\TournamentController@store')->name('admin.tournaments.store');
                Route::get('/tournaments/edit/{tournament}', 'Admin\TournamentController@edit')->name('admin.tournaments.edit')->where('tournament', '[0-9]+');
                Route::post('/tournaments/edit/{tournament}', 'Admin\TournamentController@update')->name('admin.tournaments.update')->where('tournament', '[0-9]+');
                Route::delete('/tournaments/destroy/{tournament}', 'Admin\TournamentController@destroy')->name('admin.tournaments.destroy')->where('tournament', '[0-9]+');
                Route::get('/tournaments/getPhasesAndUsers/{tournament}', 'Admin\TournamentController@getPhasesAndUsers')
                    ->name('admin.tournament.getPhasesAndUsers')->where('tournament', '[0-9]+');

                /* PHASES */
                Route::get('/phases/index', 'Admin\PhaseController@index')->name('admin.phases.index');
                Route::get('/phases/add', 'Admin\PhaseController@add')->name('admin.phases.add');
                Route::post('/phases/store/', 'Admin\PhaseController@store')->name('admin.phases.store')->where('phase', '[0-9]+');
                Route::get('/phases/edit/{phase}', 'Admin\PhaseController@edit')->name('admin.phases.edit')->where('phase', '[0-9]+');
                Route::post('/phases/{phase}', 'Admin\PhaseController@update')->name('admin.phases.update')->where('phase', '[0-9]+');
                Route::delete('/phases/destroy/{phase}', 'Admin\PhaseController@destroy')->name('admin.phases.destroy')->where('phase', '[0-9]+');

                /* GROUPS */
                Route::get('/groups/index', 'Admin\GroupController@index')->name('admin.groups.index');
                Route::get('/groups/add', 'Admin\GroupController@add')->name('admin.groups.add');
                Route::post('/groups/store/', 'Admin\GroupController@store')->name('admin.groups.store')->where('group', '[0-9]+');
                Route::get('/groups/edit/{group}', 'Admin\GroupController@edit')->name('admin.groups.edit')->where('group', '[0-9]+');
                Route::post('/groups/edit/{group}', 'Admin\GroupController@update')->name('admin.groups.update')->where('group', '[0-9]+');
                Route::delete('/groups/destroy/{group}', 'Admin\GroupController@destroy')->name('admin.groups.destroy')->where('group', '[0-9]+');

                /* MISC */
                Route::get('/calculate', 'AdminController@calculate')->name('admin.calculate');
                Route::post('/postcalculate', 'AdminController@postcalculate')->name('admin.postcalculate');
                Route::post('/undocalculate', 'AdminController@undocalculate')->name('admin.undocalculate');

                Route::get('/draw/{tournament}', 'AdminController@draw')->name('admin.draw');
                Route::get('/draw2024/{tournament}', 'AdminController@draw2024')->name('admin.draw2024');

                Route::get('/draw_second_phase/{tournament}', 'AdminController@draw_second_phase')->name('admin.draw_second_phase');

                /* Hall of Fame */
                Route::get('/hall_of_fames', [HallOfFameController::class, 'index'])->name('admin.hall_of_fames.index');
                Route::get('/hall_of_fames/create', [HallOfFameController::class, 'create'])->name('admin.hall_of_fames.create');
                Route::post('/hall_of_fames', [HallOfFameController::class, 'store'])->name('admin.hall_of_fames.store');
                Route::get('/hall_of_fames/{hallOfFame}', [HallOfFameController::class, 'show'])->name('admin.hall_of_fames.show');
                Route::get('/hall_of_fames/{hallOfFame}/edit', [HallOfFameController::class, 'edit'])->name('admin.hall_of_fames.edit');
                Route::put('/hall_of_fames/{hallOfFame}', [HallOfFameController::class, 'update'])->name('admin.hall_of_fames.update');
                Route::delete('/hall_of_fames/{hallOfFame}', [HallOfFameController::class, 'destroy'])->name('admin.hall_of_fames.destroy');
                Route::get('/hall_of_fame/add_highest_odd_win_row', [HallOfFameController::class, 'addHighestOddWinRow'])->name('admin.hall_of_fame.add_highest_odd_win_row');
            });
        });

        /* COMMENTS */
        Route::post('/comments/add', 'AjaxController\AjaxCommentController@store')->name('ajaxComments.store');
        Route::post('/comments/delete/', 'AjaxController\AjaxCommentController@delete')->name('ajaxComments.delete');

        /* USER */
        Route::post('/users/remove_avatar/', 'AjaxController\AjaxUserController@removeAvatar')->name('ajaxUsers.remove_avatar');

        /* PROFILE */
        Route::get('/profile/edit', 'User\UserController@edit')->name('auth.user.edit');
        Route::put('/profile/update', 'User\UserController@update')->name('auth.user.update');
    });
});
