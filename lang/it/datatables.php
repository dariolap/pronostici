<?php
return [
    "sSearch" => "Cerca",
    "sZeroRecords" => "La ricerca non ha portato alcun risultato.",
    "sFirst" => "Inizio",
    "sPrevious" => "Precedente",
    "sNext" => "Successivo",
    "sLast" => "Fine"
];