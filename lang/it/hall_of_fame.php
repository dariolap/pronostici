<?php

return [
    'actions' => 'Azioni',
    'total_best_odd_win' => 'Migliore quota vincente',
    'best_odd_win_users' => 'Utenti con la miglior quota vincente',
    'best_odd_win_description' => 'Descrizione miglior quota vincente',
    'create_entry' => 'Crea una nuova voce nell\'Albo d\'Oro',
    'first_points' => 'Utente primo punteggio totale',
    'first_users' => 'Primo',
    'high_prediction_win_users' => 'Utente con più pronostici indovinati',
    'id' => 'id',
    'second_points' => 'Utente secondo punteggio totale',
    'second_users' => 'Secondo',
    'third_points' => 'Utente terzo punteggio totale',
    'third_users' => 'Terzo',
    'total_high_prediction_win' => 'Totale pronostici indovinati',
    'tournament' => 'Torneo',
    'total_points_first' => 'Miglior punteggio',
    'total_points_second' => 'Secondo miglior punteggio',
    'total_points_third' => 'Terzo miglior punteggio',
    'admin_add_hall_of_fame' => 'Admin - Aggiungi albo d\'oro',
    'admin_edit_hall_of_fame' => 'Admin - Modifica albo d\'oro'
];
