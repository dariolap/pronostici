<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hall_of_fames', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('tournament_id'); // Change this line

            $table->json('first_user_ids')->nullable();
            $table->json('second_user_ids')->nullable();
            $table->json('third_user_ids')->nullable();

            $table->json('first_points_user_ids')->nullable();
            $table->float('total_points_first')->nullable();

            $table->json('second_points_user_ids')->nullable();
            $table->float('total_points_second')->nullable();

            $table->json('third_points_user_ids')->nullable();
            $table->float('total_points_third')->nullable();

            $table->json('best_odd_wins')->nullable();
            $table->unsignedInteger('total_best_odd_win')->nullable();

            $table->json('high_prediction_win_user_ids')->nullable();
            $table->unsignedInteger('total_high_prediction_win')->nullable();

            $table->timestamps();

            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hall_of_fames');
    }
};
