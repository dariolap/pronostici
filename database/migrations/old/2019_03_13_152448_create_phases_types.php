<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhasesTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // groups
        Schema::create('phase_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->timestamps();
        });

        Schema::table('phases', function (Blueprint $table) {
            $table->unsignedInteger('type_id')->nullable()->after('n_qualified');
            $table->string('name')->nullable()->after('n_qualified');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phase_types');
        Schema::table('phases', function (Blueprint $table) {
            $table->dropColumn('type_id');
            $table->dropColumn('name');
        });
    }
}