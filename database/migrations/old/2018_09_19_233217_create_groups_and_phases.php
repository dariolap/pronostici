<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsAndPhases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // groups
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tournament_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('group', 16);
            $table->timestamps();
        });

        // phases
        Schema::create('phases', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('phase')->unsigned();
            $table->integer('tournament_id')->unsigned();
            $table->integer('n_qualified')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
        Schema::dropIfExists('phases');
    }
}
