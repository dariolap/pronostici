function validate_file(file) {

    var maxMBSize = 2;
    var mimes = ['image/png', 'image/jpeg', 'image/svg+xml', 'image/gif'];
    var FileSize = (file.files[0].size / (maxMBSize * 1024 * 1024)); // in MB
    var type = file.files[0].type;
    var error = false;

    $('.file-error').html('');

    if (FileSize > 2) {
        $('.file-error').append('<div>Il file inserito supera i 2 Mb</div>');
        error = true;
    }
    if ($.inArray(type, mimes) === -1) {
        $('.file-error').append('<div>Ammessi solo i seguenti formati: jpeg,png,jpg,gif,svg</div>');
        error = true;
    }

    $('button.btn-submit').prop('disabled', error);

    return error;
}

function readURL(input) {
    if (input.files && input.files[0] && !validate_file(input)) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#avatar').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$("#file").change(function() {
    readURL(this);
});

$('button.btn-remove').on('click', function (e) {
    $.ajax({
        method: "POST",
        url: config.routes[0].remove_avatar,
        data: {
            '_token': config.token,
            'user_id': config.user_id,
        }
    })
        .done(function (msg) {
            $('#avatar').attr('src', config.default_image);
        });
});


